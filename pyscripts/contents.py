"""Data for creating 'INSERT' SQL scripts too fill database"""
from functions import make_recipe

name = 'name'
columns = 'columns'
types = 'types'
fks = 'fks'
items = 'items'
p = 'D:\\\\Images\\\\DB\\\\'

t_rarities = {name: 'rarities', columns: ['color', 'name'], types: ['int', 'str'], fks: [None, None],
              items: [('808080', 'Gray'),
                      ('ffffff', 'White'),
                      ('83aaff', 'Blue'),
                      ('80fd6c', 'Green'),
                      ('fdc46c', 'Orange'),
                      ('fb6161', 'Light red'),
                      ('ff62c9', 'Pink'),
                      ('da61fb', 'Light purple'),
                      ('67ff79', 'Lime'),
                      ('fff300', 'Yellow'),
                      ('00ffff', 'Cyan'),
                      ('ff0000', 'Red'),
                      ('8000ff', 'Purple'),
                      ('-1', 'Rainbow'),
                      ('-2', 'Fiery-red'),
                      ('ffbf00', 'Amber')]
              }

t_rarities[items] = [(str(int(item[0], base=16)), item[1]) for item in t_rarities['items']]

t_item_types = {name: 'item_types', columns: ['name', 'description'], types: ['str', 'str'], fks: [None, None],
                items: [('Material', 'Used to make other items'),
                        ('Weapon', 'Use it to attack enemies'),
                        ('Armor', 'Wear it to get protection'),
                        ('Accessory', 'Makes your cooler'),
                        ('Instrument', 'Does some useful stuff'),
                        ('Potion', 'Drink it to get buffs'),
                        ('Block', 'Can be placed'),
                        ('Furniture', 'NPC need it'),
                        ('Quest item', 'You can give it to NPC to get reward'),
                        ('Misc', 'May be it useful, may be not... Who knows?')]
                }

t_stack_sizes = {name: 'stack_sizes', columns: ['value'], types: ['int'], fks: [None],
                 items: [(1, ), (2, ), (5, ), (30, ), (50, ), (75, ), (99, ), (100, ), (999, ), (1000, )]}

t_images = {name: 'images', columns: ['path'], types: ['str'], fks: [None],
            items: [(p + 'Shield_of_Cthulhu.png', ),
                    (p + 'Terra_Blade.png', ),
                    (p + 'Copper_Shortsword.png', ),
                    (p + 'Wood.png', ),
                    (p + 'Stone_Block.png', ),
                    (p + 'Copper_Pickaxe.png', ),
                    (p + 'Copper_Axe.png', ),
                    (p + 'Copper_Broadsword.png', ),
                    (p + 'Copper_Bar.png', ),
                    (p + 'Copper_Ore.png', ),
                    (p + 'Wooden_Sword.png', ),
                    (p + 'Wooden_Hammer.png', ),
                    (p + 'Work_Bench.png', ),
                    (p + 'Furnace.png', ),
                    (p + 'Furnace_placed.gif', ),
                    (p + 'Iron_Anvil.png', ),
                    (p + 'Torch.png', ),
                    (p + 'Gel.png', ),
                    (p + 'Regeneration.png', ),
                    (p + 'Regeneration_Potion.png', )]
            }

t_items = {name: 'items',
           columns: ['name', 'description', 'rarity_id', 'item_type_id', 'base_price', 'stack_size', 'image_id'],
           types: ['str', 'str', 'fk', 'fk', 'int', 'int', 'fk'],
           fks: [None, None, ('id', 'rarities', 'name', 'str'), ('id', 'item_types', 'name', 'str'), None, None, ('id', 'images', 'path', 'str')],
           items: [('Wood', 'Just Wood', 'White', 'Material', 0, 999, p + 'Wood.png'),
                   ('Stone', 'Gray and boring', 'White', 'Block', 0, 999, p + 'Stone_Block.png'),
                   ('Wooden Sword', 'Better than nothing', 'White', 'Weapon', 20, 1, p + 'Wooden_Sword.png'),
                   ('Wooden Hammer', 'Stop! Hammer time!', 'White', 'Instrument', 10, 1, p + 'Wooden_Hammer.png'),
                   ('Copper Ore', 'Smelt it', 'White', 'Block', 50, 999, p + 'Copper_Ore.png'),
                   ('Copper Bar', 'Sadly, not a gold', 'White', 'Material', 150, 999, p + 'Copper_Bar.png'),
                   ('Copper Pickaxe', 'Digidy-dig!', 'White', 'Instrument', 100, 1, p + 'Copper_Pickaxe.png'),
                   ('Copper Axe', 'Chop-chop', 'White', 'Instrument', 80, 1, p + 'Copper_Axe.png'),
                   ('Copper Sword', 'Better than wooden', 'White', 'Weapon', 90, 1, p + 'Copper_Broadsword.png'),
                   ('Copper Shortsword', 'Stab', 'White', 'Weapon', 35, 1, p + 'Copper_Shortsword.png'),
                   ('Terra Blade', 'Try to get', 'Yellow', 'Weapon', 200000, 1, p + 'Terra_Blade.png'),
                   ('Shield of Cthulhu', 'Dash!', 'Rainbow', 'Accessory', 20000, 1, p + 'Shield_of_Cthulhu.png'),
                   ('Work Bench', 'Crafting crafting crafting', 'White', 'Furniture', 0, 99, p + 'Work_Bench.png'),
                   ('Furnace', 'Hot stuff', 'White', 'Furniture', 0, 99, p + 'Furnace.png'),
                   ('Anvil', 'Strike while the iron is hot', 'White', 'Furniture', 0, 99, p + 'Iron_Anvil.png'),
                   ('Torch', 'We got torches', 'White', 'Furniture', 0, 999, p + 'Torch.png'),
                   ('Gel', 'Both tasty and flammable', 'White', 'Material', 0, 999, p + 'Gel.png'),
                   ('Regeneration Potion', 'Provides life regeneration', 'Blue', 'Potion', 200, 30, p + 'Regeneration_Potion.png')]
           }

t_defense_types = {name: 'defense_types', columns: ['name'], types: ['str'], fks: [None],
                   items: [('Melee',), ('Ranged',), ('Magic',), ('Summon',), ('Rogue',), ('Radiant',), ('Standard',)]}

t_damage_types = {name: 'damage_types', columns: ['name', 'defense_type_id'], types: ['str', 'fk'],
                  fks: [None, ('id', 'defense_types', 'name', 'str')],
                  items: [('Melee', 'Melee'),
                          ('Ranged', 'Ranged'),
                          ('Magic', 'Magic'),
                          ('Summon', 'Summon'),
                          ('Rogue', 'Rogue'),
                          ('Radiant', 'Radiant'),
                          ('True', 'NULL')]
                  }

t_items_to_defense_types = {name: 'items_to_defense_types',
                            columns: ['item_id', 'defense_type_id', 'value'],
                            types: ['fk', 'fk', 'int'],
                            fks: [('id', 'items', 'name', 'str'), ('id', 'defense_types', 'name', 'str'), None],
                            items: [('Shield of Cthulhu', 'Standard', 2)]
                            }

t_items_to_damage_types = {name: 'items_to_damage_types',
                           columns: ['item_id', 'damage_type_id', 'value'],
                           types: ['fk', 'fk', 'int'],
                           fks: [('id', 'items', 'name', 'str'), ('id', 'damage_types', 'name', 'str'), None],
                           items: [('Shield of Cthulhu', 'Melee', 30),
                                   ('Copper Sword', 'Melee', 8),
                                   ('Copper Shortsword', 'Melee', 5),
                                   ('Copper Pickaxe', 'Melee', 4),
                                   ('Copper Axe', 'Melee', 3),
                                   ('Wooden Sword', 'Melee', 7),
                                   ('Wooden Hammer', 'Melee', 2),
                                   ('Terra Blade', 'Melee', 95)]
                           }

t_character_classes = {name: 'character_classes', columns: ['name'], types: ['str'], fks: [None],
                       items: [('Warrior',), ('Ranger',), ('Mage',), ('Summoner',), ('Rogue',), ('Healer',)]}

t_items_to_character_classes = {name: 'items_to_character_classes',
                                columns: ['item_id', 'class_id'],
                                types: ['fk', 'fk'],
                                fks: [('id', 'items', 'name', 'str'), ('id', 'character_classes', 'name', 'str')],
                                items: [('Wooden Sword', 'Warrior'),
                                        ('Copper Sword', 'Warrior'),
                                        ('Copper Shortsword', 'Warrior'),
                                        ('Copper Shortsword', 'Rogue'),
                                        ('Terra Blade', 'Warrior'),
                                        ('Shield of Cthulhu', 'Warrior'),
                                        ('Shield of Cthulhu', 'Rogue')]
                                }

t_effect_types = {name: 'effect_types', columns: ['name'], types: ['str'], fks: [None],
                  items: [('Buff', ), ('Debuff', ), ('Pet', ), ('Minion', ), ('Status Effect', ), ('Curse', ), ('Imbue', )]}

t_effects = {name: 'effects',
             columns: ['name', 'description', 'image_id', 'effect_type_id'],
             types: ['str', 'str', 'fk', 'fk'],
             fks: [None, None, ('id', 'images', 'path', 'str'), ('id', 'effect_types', 'name', 'str')],
             items: [('Regeneration', 'Provides life regeneration', p + 'Regeneration.png', 'Buff')]}

t_items_to_effects = {name: 'items_to_effects',
                      columns: ['item_id', 'effect_id', 'duration'],
                      types: ['fk', 'fk', 'int'],
                      fks: [('id', 'items', 'name', 'str'), ('id', 'effects', 'name', 'str'), None],
                      items: [('Regeneration Potion', 'Regeneration', 480)]}

t_workbenches = {name: 'workbenches', columns: ['name', 'image_id'], types: ['str', 'fk'], fks: [None, ('id', 'images', 'path', 'str')],
                 items: [('By hand', p + 'NULL'),
                         ('Work Bench', p + 'Work_Bench.png'),
                         ('Furnace', p + 'Furnace_placed.gif'),
                         ('Anvil', p + 'Iron_Anvil.png')]
                 }

t_workbenches_to_items = {name: 'workbenches_to_items',
                          columns: ['workbench_id', 'item_id'],
                          types: ['fk', 'fk'],
                          fks: [('id', 'workbenches', 'name', 'str'), ('id', 'items', 'name', 'str')],
                          items: [('Work Bench', 'Work Bench'),
                                  ('Furnace', 'Furnace'),
                                  ('Anvil', 'Anvil')]
                          }

t_recipes = {name: 'recipes',
             columns: ['product_id', 'product_amount'],
             types: ['fk', 'int'],
             fks: [('id', 'items', 'name', 'str'), None],
             items: []
             }

t_recipes_to_ingredients = {name: 'recipes_to_ingredients',
                            columns: ['recipe_id', 'item_id', 'amount'],
                            types: ['fk', 'fk', 'int'],
                            fks: [('id', 'recipes', 'product_id', 'int'), ('id', 'items', 'name', 'str'), None],
                            items: []
                            }

t_recipes_to_workbenches = {name: 'recipes_to_workbenches',
                            columns: ['recipe_id', 'workbench_id'],
                            types: ['fk', 'fk'],
                            fks: [('id', 'recipes', 'product_id', 'int'), ('id', 'workbenches', 'name', 'str')],
                            items: []
                            }

recipes = [('Work Bench',           1,  [('Wood', 10)],                                 ['By hand']),
           ('Furnace',              1,  [('Wood', 4), ('Stone', 20), ('Torch', 3)],     ['Work Bench']),
           ('Wooden Sword',         1,  [('Wood', 7)],                                  ['Work Bench']),
           ('Wooden Hammer',        1,  [('Wood', 8)],                                  ['Work Bench']),
           ('Copper Bar',           1,  [('Copper Ore', 3)],                            ['Furnace']),
           ('Copper Pickaxe',       1,  [('Copper Bar', 12), ('Wood', 4)],              ['Anvil']),
           ('Copper Axe',           1,  [('Copper Bar', 9), ('Wood', 3)],               ['Anvil']),
           ('Copper Sword',         1,  [('Copper Bar', 8)],                            ['Anvil']),
           ('Copper Shortsword',    1,  [('Copper Bar', 7)],                            ['Anvil']),
           ('Torch',                3,  [('Wood', 1), ('Gel', 1)],                      ['By hand'])]

for rec in recipes:
    r, i, w = make_recipe(*rec)
    t_recipes[items].append(r)
    t_recipes_to_ingredients[items] += i
    t_recipes_to_workbenches[items] += w

content_list = [t_rarities,
                t_item_types,
                t_stack_sizes,
                t_images,
                t_items,
                t_defense_types,
                t_damage_types,
                t_items_to_defense_types,
                t_items_to_damage_types,
                t_character_classes,
                t_items_to_character_classes,
                t_effect_types,
                t_effects,
                t_items_to_effects,
                t_workbenches,
                t_workbenches_to_items,
                t_recipes,
                t_recipes_to_ingredients,
                t_recipes_to_workbenches]


b_images = {name: 'images', columns: ['path'], types: ['str'], fks: [None],
            items: []}
# (p + '')

b_items = {name: 'items',
           columns: ['name', 'description', 'rarity_id', 'item_type_id', 'base_price', 'stack_size', 'image_id'],
           types: ['str', 'str', 'fk', 'fk', 'int', 'int', 'fk'],
           fks: [None, None, ('id', 'rarities', 'name', 'str'), ('id', 'item_types', 'name', 'str'), None, None, ('id', 'images', 'path', 'str')],
           items: []}
# ('Regeneration Potion', 'Provides life regeneration', 'Blue', 'Potion', 200, 30, p + 'Regeneration_Potion.png')

b_effects = {name: 'effects',
             columns: ['name', 'description', 'image_id', 'effect_type_id'],
             types: ['str', 'str', 'fk', 'fk'],
             fks: [None, None, ('id', 'images', 'path', 'str'), ('id', 'effect_types', 'name', 'str')],
             items: []}
# ('Regeneration', 'Provides life regeneration', p + 'Regeneration.png', 'Buff')

b_items_to_effects = {name: 'items_to_effects',
                      columns: ['item_id', 'effect_id', 'duration'],
                      types: ['fk', 'fk', 'int'],
                      fks: [('id', 'items', 'name', 'str'), ('id', 'effects', 'name', 'str'), None],
                      items: []}
# ('Regeneration Potion', 'Regeneration', 480)
