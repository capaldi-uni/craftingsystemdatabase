CREATE DATABASE  IF NOT EXISTS `crafting_db` /*!40100 DEFAULT CHARACTER SET utf8 */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `crafting_db`;
-- MySQL dump 10.13  Distrib 8.0.21, for Win64 (x86_64)
--
-- Host: localhost    Database: crafting_db
-- ------------------------------------------------------
-- Server version	8.0.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `character_classes`
--

DROP TABLE IF EXISTS `character_classes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `character_classes` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `character_classes`
--

LOCK TABLES `character_classes` WRITE;
/*!40000 ALTER TABLE `character_classes` DISABLE KEYS */;
INSERT INTO `character_classes` VALUES (1,'Warrior'),(2,'Ranger'),(3,'Mage'),(4,'Summoner'),(5,'Rogue'),(6,'Healer');
/*!40000 ALTER TABLE `character_classes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `damage_types`
--

DROP TABLE IF EXISTS `damage_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `damage_types` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `defense_type_id` int unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `defense_type_id_idx` (`defense_type_id`),
  CONSTRAINT `defense_type_id_dm` FOREIGN KEY (`defense_type_id`) REFERENCES `defense_types` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `damage_types`
--

LOCK TABLES `damage_types` WRITE;
/*!40000 ALTER TABLE `damage_types` DISABLE KEYS */;
INSERT INTO `damage_types` VALUES (1,'Melee',1),(2,'Ranged',2),(3,'Magic',3),(4,'Summon',4),(5,'Rogue',5),(6,'Radiant',6),(7,'True',NULL);
/*!40000 ALTER TABLE `damage_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `defense_types`
--

DROP TABLE IF EXISTS `defense_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `defense_types` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `defense_types`
--

LOCK TABLES `defense_types` WRITE;
/*!40000 ALTER TABLE `defense_types` DISABLE KEYS */;
INSERT INTO `defense_types` VALUES (1,'Melee'),(2,'Ranged'),(3,'Magic'),(4,'Summon'),(5,'Rogue'),(6,'Radiant'),(7,'Standard');
/*!40000 ALTER TABLE `defense_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `effect_types`
--

DROP TABLE IF EXISTS `effect_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `effect_types` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `effect_types`
--

LOCK TABLES `effect_types` WRITE;
/*!40000 ALTER TABLE `effect_types` DISABLE KEYS */;
INSERT INTO `effect_types` VALUES (1,'Buff'),(2,'Debuff'),(3,'Pet'),(4,'Minion'),(5,'Status Effect'),(6,'Curse'),(7,'Imbue');
/*!40000 ALTER TABLE `effect_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `effects`
--

DROP TABLE IF EXISTS `effects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `effects` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `image_id` int unsigned DEFAULT NULL,
  `effect_type_id` int unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `effect_type_id_idx` (`effect_type_id`),
  KEY `image_idx` (`image_id`),
  CONSTRAINT `effect_type_id_e` FOREIGN KEY (`effect_type_id`) REFERENCES `effect_types` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `image_id_e` FOREIGN KEY (`image_id`) REFERENCES `images` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `effects`
--

LOCK TABLES `effects` WRITE;
/*!40000 ALTER TABLE `effects` DISABLE KEYS */;
INSERT INTO `effects` VALUES (1,'Regeneration','Provides life regeneration',19,1),(2,'Ironskin','Increase defense by 8',21,1),(3,'Gravitation','Press UP to reverse gravity',23,1),(4,'Holy Protection','You will dodge the next attack',25,1),(5,'Poisoned','Slowly losing life',26,2),(6,'Chaos State','Using the Rod of Discord will take life',27,2);
/*!40000 ALTER TABLE `effects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `images`
--

DROP TABLE IF EXISTS `images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `images` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `path` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `images`
--

LOCK TABLES `images` WRITE;
/*!40000 ALTER TABLE `images` DISABLE KEYS */;
INSERT INTO `images` VALUES (1,'D:\\Images\\DB\\Shield_of_Cthulhu.png'),(2,'D:\\Images\\DB\\Terra_Blade.png'),(3,'D:\\Images\\DB\\Copper_Shortsword.png'),(4,'D:\\Images\\DB\\Wood.png'),(5,'D:\\Images\\DB\\Stone_Block.png'),(6,'D:\\Images\\DB\\Copper_Pickaxe.png'),(7,'D:\\Images\\DB\\Copper_Axe.png'),(8,'D:\\Images\\DB\\Copper_Broadsword.png'),(9,'D:\\Images\\DB\\Copper_Bar.png'),(10,'D:\\Images\\DB\\Copper_Ore.png'),(11,'D:\\Images\\DB\\Wooden_Sword.png'),(12,'D:\\Images\\DB\\Wooden_Hammer.png'),(13,'D:\\Images\\DB\\Work_Bench.png'),(14,'D:\\Images\\DB\\Furnace.png'),(15,'D:\\Images\\DB\\Furnace_placed.gif'),(16,'D:\\Images\\DB\\Iron_Anvil.png'),(17,'D:\\Images\\DB\\Torch.png'),(18,'D:\\Images\\DB\\Gel.png'),(19,'D:\\Images\\DB\\Regeneration.png'),(20,'D:\\Images\\DB\\Regeneration_Potion.png'),(21,'D:\\Images\\DB\\Ironskin.png'),(22,'D:\\Images\\DB\\Ironskin_Potion.png'),(23,'D:\\Images\\DB\\Gravitation.png'),(24,'D:\\Images\\DB\\Gravitation_Potion.png'),(25,'D:\\Images\\DB\\Holy_Protection.png'),(26,'D:\\Images\\DB\\Poisoned.png'),(27,'D:\\Images\\DB\\Chaos_State.png'),(28,'D:\\Images\\DB\\Rod_of_Discord.png');
/*!40000 ALTER TABLE `images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_types`
--

DROP TABLE IF EXISTS `item_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `item_types` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_types`
--

LOCK TABLES `item_types` WRITE;
/*!40000 ALTER TABLE `item_types` DISABLE KEYS */;
INSERT INTO `item_types` VALUES (1,'Material','Used to make other items'),(2,'Weapon','Use it to attack enemies'),(3,'Armor','Wear it to get protection'),(4,'Accessory','Makes your cooler'),(5,'Tool','Does some useful stuff'),(6,'Potion','Drink it to get buffs'),(7,'Block','Can be placed'),(8,'Furniture','NPC need it'),(9,'Quest item','You can give it to NPC to get reward'),(10,'Misc','May be it useful, may be not... Who knows?');
/*!40000 ALTER TABLE `item_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `items`
--

DROP TABLE IF EXISTS `items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `items` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `image_id` int unsigned DEFAULT NULL,
  `base_price` int unsigned NOT NULL,
  `rarity_id` int unsigned NOT NULL,
  `item_type_id` int unsigned NOT NULL,
  `stack_size` int unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `rarity_id_idx` (`rarity_id`),
  KEY `item_type_id_idx` (`item_type_id`),
  KEY `stack_size_idx` (`stack_size`),
  KEY `image_idx` (`image_id`),
  CONSTRAINT `image_id_i` FOREIGN KEY (`image_id`) REFERENCES `images` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `item_type_id_i` FOREIGN KEY (`item_type_id`) REFERENCES `item_types` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `rarity_id_i` FOREIGN KEY (`rarity_id`) REFERENCES `rarities` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `stack_size_i` FOREIGN KEY (`stack_size`) REFERENCES `stack_sizes` (`value`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `items`
--

LOCK TABLES `items` WRITE;
/*!40000 ALTER TABLE `items` DISABLE KEYS */;
INSERT INTO `items` VALUES (1,'Wood','Can be placed',4,0,2,1,999),(2,'Stone','Can be placed',5,0,2,7,999),(3,'Wooden Sword','Better than nothing',11,20,2,2,1),(4,'Wooden Hammer','Stop! Hammer time!',12,10,2,5,1),(5,'Copper Ore','Smelt it',10,50,2,7,999),(6,'Copper Bar','Sadly, not a gold',9,150,2,1,999),(7,'Copper Pickaxe','Digidy-dig!',6,100,2,5,1),(8,'Copper Axe','Chop-chop',7,80,2,5,1),(9,'Copper Sword','Better than wooden',8,90,2,2,1),(10,'Copper Shortsword','Stab',3,35,2,2,1),(11,'Terra Blade','Try to get',2,200000,10,2,1),(12,'Shield of Cthulhu','Dash!',1,20000,14,4,1),(13,'Work Bench','Crafting crafting crafting',13,0,2,8,99),(14,'Furnace','Hot stuff',14,0,2,8,99),(15,'Anvil','Strike while the iron is hot',16,0,2,8,99),(16,'Torch','We got torches',17,0,2,8,999),(17,'Gel','Both tasty and flammable',18,0,2,1,999),(18,'Regeneration Potion','Provides life regeneration',20,200,3,6,30),(19,'Ironskin Potion','Increase defense by 8',22,200,3,6,30),(20,'Gravitation Potion','Allows the control of gravity',24,200,3,6,30),(21,'Rod of Discord','Teleports you to the position of the mouse',28,100000,9,5,1);
/*!40000 ALTER TABLE `items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `items_to_character_classes`
--

DROP TABLE IF EXISTS `items_to_character_classes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `items_to_character_classes` (
  `item_id` int unsigned NOT NULL,
  `class_id` int unsigned NOT NULL,
  PRIMARY KEY (`item_id`,`class_id`),
  KEY `item_id_idx` (`item_id`),
  KEY `class_id_idx` (`class_id`),
  CONSTRAINT `class_id_i` FOREIGN KEY (`class_id`) REFERENCES `character_classes` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `item_id_c` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `items_to_character_classes`
--

LOCK TABLES `items_to_character_classes` WRITE;
/*!40000 ALTER TABLE `items_to_character_classes` DISABLE KEYS */;
INSERT INTO `items_to_character_classes` VALUES (3,1),(9,1),(10,1),(10,5),(11,1),(12,1),(12,5);
/*!40000 ALTER TABLE `items_to_character_classes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `items_to_damage_types`
--

DROP TABLE IF EXISTS `items_to_damage_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `items_to_damage_types` (
  `item_id` int unsigned NOT NULL,
  `damage_type_id` int unsigned NOT NULL,
  `value` int unsigned NOT NULL,
  PRIMARY KEY (`item_id`,`damage_type_id`),
  KEY `item_id_idx` (`item_id`),
  KEY `damage_type_idx` (`damage_type_id`),
  CONSTRAINT `damage_type_id_i` FOREIGN KEY (`damage_type_id`) REFERENCES `damage_types` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `item_id_dm` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `items_to_damage_types`
--

LOCK TABLES `items_to_damage_types` WRITE;
/*!40000 ALTER TABLE `items_to_damage_types` DISABLE KEYS */;
INSERT INTO `items_to_damage_types` VALUES (3,1,7),(4,1,2),(7,1,4),(8,1,3),(9,1,8),(10,1,5),(11,1,95),(12,1,30);
/*!40000 ALTER TABLE `items_to_damage_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `items_to_defense_types`
--

DROP TABLE IF EXISTS `items_to_defense_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `items_to_defense_types` (
  `item_id` int unsigned NOT NULL,
  `defense_type_id` int unsigned NOT NULL,
  `value` int unsigned NOT NULL,
  PRIMARY KEY (`item_id`,`defense_type_id`),
  KEY `item_id_idx` (`item_id`),
  KEY `defense_type_id_idx` (`defense_type_id`),
  CONSTRAINT `defense_type_id_i` FOREIGN KEY (`defense_type_id`) REFERENCES `defense_types` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `item_id_df` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `items_to_defense_types`
--

LOCK TABLES `items_to_defense_types` WRITE;
/*!40000 ALTER TABLE `items_to_defense_types` DISABLE KEYS */;
INSERT INTO `items_to_defense_types` VALUES (12,7,2);
/*!40000 ALTER TABLE `items_to_defense_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `items_to_effects`
--

DROP TABLE IF EXISTS `items_to_effects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `items_to_effects` (
  `item_id` int unsigned NOT NULL,
  `effect_id` int unsigned NOT NULL,
  `duration` int unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`item_id`,`effect_id`),
  KEY `item_id_idx` (`item_id`),
  KEY `effect_id_idx` (`effect_id`),
  CONSTRAINT `effect_id_i` FOREIGN KEY (`effect_id`) REFERENCES `effects` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `item_id_e` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `items_to_effects`
--

LOCK TABLES `items_to_effects` WRITE;
/*!40000 ALTER TABLE `items_to_effects` DISABLE KEYS */;
INSERT INTO `items_to_effects` VALUES (18,1,480),(19,2,480),(20,3,180),(21,6,6);
/*!40000 ALTER TABLE `items_to_effects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rarities`
--

DROP TABLE IF EXISTS `rarities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rarities` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `color` int NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rarities`
--

LOCK TABLES `rarities` WRITE;
/*!40000 ALTER TABLE `rarities` DISABLE KEYS */;
INSERT INTO `rarities` VALUES (1,8421504,'Gray'),(2,16777215,'White'),(3,8628991,'Blue'),(4,8453484,'Green'),(5,16630892,'Orange'),(6,16474465,'Light red'),(7,16736969,'Pink'),(8,14311931,'Light purple'),(9,6815609,'Lime'),(10,16773888,'Yellow'),(11,65535,'Cyan'),(12,16711680,'Red'),(13,8388863,'Purple'),(14,-1,'Rainbow'),(15,-2,'Fiery-red'),(16,16760576,'Amber');
/*!40000 ALTER TABLE `rarities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `recipes`
--

DROP TABLE IF EXISTS `recipes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `recipes` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int unsigned NOT NULL,
  `product_amount` int unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `item_id_idx` (`product_id`),
  CONSTRAINT `item_id_pr` FOREIGN KEY (`product_id`) REFERENCES `items` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `recipes`
--

LOCK TABLES `recipes` WRITE;
/*!40000 ALTER TABLE `recipes` DISABLE KEYS */;
INSERT INTO `recipes` VALUES (1,13,1),(2,14,1),(3,3,1),(4,4,1),(5,6,1),(6,7,1),(7,8,1),(8,9,1),(9,10,1),(10,16,3);
/*!40000 ALTER TABLE `recipes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `recipes_to_ingredients`
--

DROP TABLE IF EXISTS `recipes_to_ingredients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `recipes_to_ingredients` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `recipe_id` int unsigned NOT NULL,
  `item_id` int unsigned NOT NULL,
  `amount` int unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `recipe_id_idx` (`recipe_id`),
  KEY `item_id_idx` (`item_id`),
  CONSTRAINT `item_id_r` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `recipe_id_i` FOREIGN KEY (`recipe_id`) REFERENCES `recipes` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `recipes_to_ingredients`
--

LOCK TABLES `recipes_to_ingredients` WRITE;
/*!40000 ALTER TABLE `recipes_to_ingredients` DISABLE KEYS */;
INSERT INTO `recipes_to_ingredients` VALUES (1,1,1,10),(2,2,1,4),(3,2,2,20),(4,2,16,3),(5,3,1,7),(6,4,1,8),(7,5,5,3),(8,6,6,12),(9,6,1,4),(10,7,6,9),(11,7,1,3),(12,8,6,8),(13,9,6,7),(14,10,1,1),(15,10,17,1);
/*!40000 ALTER TABLE `recipes_to_ingredients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `recipes_to_workbenches`
--

DROP TABLE IF EXISTS `recipes_to_workbenches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `recipes_to_workbenches` (
  `workbench_id` int unsigned NOT NULL,
  `recipe_id` int unsigned NOT NULL,
  PRIMARY KEY (`workbench_id`,`recipe_id`),
  KEY `workbench_id_idx` (`workbench_id`),
  KEY `recipe_id_idx` (`recipe_id`),
  CONSTRAINT `recipe_id` FOREIGN KEY (`recipe_id`) REFERENCES `recipes` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `workbench_id` FOREIGN KEY (`workbench_id`) REFERENCES `workbenches` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `recipes_to_workbenches`
--

LOCK TABLES `recipes_to_workbenches` WRITE;
/*!40000 ALTER TABLE `recipes_to_workbenches` DISABLE KEYS */;
INSERT INTO `recipes_to_workbenches` VALUES (1,1),(1,10),(2,2),(2,3),(2,4),(3,5),(4,6),(4,7),(4,8),(4,9);
/*!40000 ALTER TABLE `recipes_to_workbenches` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stack_sizes`
--

DROP TABLE IF EXISTS `stack_sizes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `stack_sizes` (
  `value` int unsigned NOT NULL,
  PRIMARY KEY (`value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stack_sizes`
--

LOCK TABLES `stack_sizes` WRITE;
/*!40000 ALTER TABLE `stack_sizes` DISABLE KEYS */;
INSERT INTO `stack_sizes` VALUES (1),(2),(5),(30),(50),(75),(99),(100),(999),(1000);
/*!40000 ALTER TABLE `stack_sizes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `workbenches`
--

DROP TABLE IF EXISTS `workbenches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `workbenches` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `image_id` int unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `image_idx` (`image_id`),
  CONSTRAINT `image_id_w` FOREIGN KEY (`image_id`) REFERENCES `images` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `workbenches`
--

LOCK TABLES `workbenches` WRITE;
/*!40000 ALTER TABLE `workbenches` DISABLE KEYS */;
INSERT INTO `workbenches` VALUES (1,'By hand',NULL),(2,'Work Bench',13),(3,'Furnace',15),(4,'Anvil',16);
/*!40000 ALTER TABLE `workbenches` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `workbenches_to_items`
--

DROP TABLE IF EXISTS `workbenches_to_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `workbenches_to_items` (
  `workbench_id` int unsigned NOT NULL,
  `item_id` int unsigned NOT NULL,
  PRIMARY KEY (`workbench_id`,`item_id`),
  KEY `workbench_id_idx` (`workbench_id`),
  KEY `item_id_idx` (`item_id`),
  CONSTRAINT `item_id_w` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `workbench_id_i` FOREIGN KEY (`workbench_id`) REFERENCES `workbenches` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `workbenches_to_items`
--

LOCK TABLES `workbenches_to_items` WRITE;
/*!40000 ALTER TABLE `workbenches_to_items` DISABLE KEYS */;
INSERT INTO `workbenches_to_items` VALUES (2,13),(3,14),(4,15);
/*!40000 ALTER TABLE `workbenches_to_items` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-11-30 12:49:38
