-- let's fill database with basic data

use farmershelper;

-- firstly, let's define our fields

INSERT INTO `fields` (`number`, `space`) VALUES
(1, 100), (2, 50), (3, 50);

-- next - crops types we have

INSERT INTO `crop_types` (name) VALUES
	('cereals'),
    ('vegetables'),
    ('berries'),
    ('beans'),
    ('annual'),
    ('perenial');
    
-- crops themselves

INSERT INTO `crops` (`name`) VALUES
	('wheat'),
    ('pea'),
    ('strawberry'),
    ('tomato'),
    ('grape'),
    ('corn');

-- let's connect crops with their types
-- as we don't know their ids we need database to get them for us
-- so we use SELECT querry searching for `id` of crop or type with specified value of `name` field

INSERT INTO `crops_to_types` (`crop_id`, `type_id`)  VALUES
	((SELECT `id` FROM `crops` WHERE `name`='wheat'), (SELECT `id` FROM `crop_types` WHERE `name`='annual')),
	((SELECT `id` FROM `crops` WHERE `name`='wheat'), (SELECT `id` FROM `crop_types` WHERE `name`='cereals')),
	((SELECT `id` FROM `crops` WHERE `name`='pea'), (SELECT `id` FROM `crop_types` WHERE `name`='beans')),
	((SELECT `id` FROM `crops` WHERE `name`='pea'), (SELECT `id` FROM `crop_types` WHERE `name`='annual')),
	((SELECT `id` FROM `crops` WHERE `name`='strawberry'), (SELECT `id` FROM `crop_types` WHERE `name`='berries')),
	((SELECT `id` FROM `crops` WHERE `name`='strawberry'), (SELECT `id` FROM `crop_types` WHERE `name`='perenial')),
	((SELECT `id` FROM `crops` WHERE `name`='tomato'), (SELECT `id` FROM `crop_types` WHERE `name`='vegetables')),
	((SELECT `id` FROM `crops` WHERE `name`='tomato'), (SELECT `id` FROM `crop_types` WHERE `name`='annual')),
	((SELECT `id` FROM `crops` WHERE `name`='grape'), (SELECT `id` FROM `crop_types` WHERE `name`='perenial')),
	((SELECT `id` FROM `crops` WHERE `name`='grape'), (SELECT `id` FROM `crop_types` WHERE `name`='berries')),
	((SELECT `id` FROM `crops` WHERE `name`='corn'), (SELECT `id` FROM `crop_types` WHERE `name`='annual')),
	((SELECT `id` FROM `crops` WHERE `name`='corn'), (SELECT `id` FROM `crop_types` WHERE `name`='cereals'));

-- then connect crops to fields we planting them on

INSERT INTO `crops_to_fields` (`crop_id`, `field_id`) VALUES
	((SELECT `id` FROM `crops` WHERE `name`='wheat'), (SELECT `id` FROM `fields` WHERE `number`=2)),
	((SELECT `id` FROM `crops` WHERE `name`='pea'), (SELECT `id` FROM `fields` WHERE `number`=1)),
	((SELECT `id` FROM `crops` WHERE `name`='tomato'), (SELECT `id` FROM `fields` WHERE `number`=1)),
	((SELECT `id` FROM `crops` WHERE `name`='corn'), (SELECT `id` FROM `fields` WHERE `number`=3)),
	((SELECT `id` FROM `crops` WHERE `name`='strawberry'), (SELECT `id` FROM `fields` WHERE `number`=1)),
	((SELECT `id` FROM `crops` WHERE `name`='grape'), (SELECT `id` FROM `fields` WHERE `number`=1));

-- storage time

INSERT INTO `storages` (`number`, `volume`) VALUES
	(1, 50),
    (2, 50);
    
-- fertilizers
INSERT INTO `fertilizers` (`name`) VALUES
	('Nitrogenuous'),
    ('Phosporous');
    
-- And connect them
INSERT INTO `fertilizers_to_storages` (`fertilizer_id`, `storage_id`, `volume`) VALUES
	((SELECT `id` FROM `fertilizers` WHERE `name`='Nitrogenuous'), (SELECT `id` FROM `storages` WHERE `number`=1), 40),
    ((SELECT `id` FROM `fertilizers` WHERE `name`='Phosporous'), (SELECT `id` FROM `storages` WHERE `number`=2), 45);
    
-- And fertilizers to plants
INSERT INTO `fertilizers_to_crops` (`fertilizer_id`, `crop_id`, `allowed`) VALUES
	((SELECT `id` FROM `fertilizers` WHERE `name`='Nitrogenuous'), (SELECT `id` FROM `crops` WHERE `name`='corn'), TRUE),
    ((SELECT `id` FROM `fertilizers` WHERE `name`='Nitrogenuous'), (SELECT `id` FROM `crops` WHERE `name`='wheat'), TRUE);
    
-- And, at last, which fields whe want ot fertilize
INSERT INTO `fields_to_fertilizers` (`fertilizer_id`, `field_id`, `volume`) VALUES
	((SELECT `id` FROM `fertilizers` WHERE `name`='Nitrogenuous'), (SELECT `id` FROM `fields` WHERE `number`=2), 15),
    ((SELECT `id` FROM `fertilizers` WHERE `name`='Nitrogenuous'), (SELECT `id` FROM `fields` WHERE `number`=3), 10);


    
    