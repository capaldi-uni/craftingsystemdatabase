-- Database creation

CREATE DATABASE IF NOT EXISTS farmershelper
CHARACTER SET = 'utf8';

USE farmershelper;


-- Entity tables creation

CREATE TABLE IF NOT EXISTS `plants` (
	`id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
	`name` varchar(100) NOT NULL,
	
	PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `plant_types` (
	`id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
	`name` varchar(100) NOT NULL,
	
	PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `fertilizers` (
	`id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
	`name` varchar(100) NOT NULL,
	
	PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `fields` (
	`id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
	`space` INT UNSIGNED NOT NULL,
	
	PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `storages` (
	`id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
	`volume` INT NOT NULL,
	
	PRIMARY KEY (`id`)
);


-- Many-to-many relationship tables creation

CREATE TABLE IF NOT EXISTS `plants_to_types` (
	`id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
	`plant_id` INT UNSIGNED NOT NULL,
	`type_id` INT UNSIGNED NOT NULL,
	
	PRIMARY KEY (`id`), 
	
	CONSTRAINT `plants_to_types_plants` FOREIGN KEY (`plant_id`) REFERENCES `plants`(`id`),
	CONSTRAINT `plants_to_types_types` FOREIGN KEY (`type_id`) REFERENCES `plant_types`(`id`)
);

CREATE TABLE IF NOT EXISTS `plants_to_fields` (
	`id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
	`plant_id` INT UNSIGNED NOT NULL,
	`field_id` INT UNSIGNED NOT NULL,
	
	PRIMARY KEY (`id`),
	
	CONSTRAINT `plants_to_fields_plants` FOREIGN KEY (`plant_id`) REFERENCES `plants`(`id`),
	CONSTRAINT `plants_to_fields_fields` FOREIGN KEY (`field_id`) REFERENCES `fields`(`id`)
);

CREATE TABLE IF NOT EXISTS `fields_to_fertilizers` (
	`id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
	`field_id` INT UNSIGNED NOT NULL,
	`fertilizer_id` INT UNSIGNED NOT NULL,
	
	PRIMARY KEY (`id`),
	
	CONSTRAINT `fields_to_fertilizers_fields` FOREIGN KEY (`field_id`) REFERENCES `fields`(`id`),
	CONSTRAINT `fields_to_fertilizers_fertilizers` FOREIGN KEY (`fertilizer_id`) REFERENCES `fertilizers`(`id`)
);

CREATE TABLE IF NOT EXISTS `fertilizers_to_plants` (
	`id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
	`fertilizer_id` INT UNSIGNED NOT NULL,
	`plant_id` INT UNSIGNED NOT NULL,
	`allowed` BOOLEAN NOT NULL,
	
	PRIMARY KEY (`id`),
	
	CONSTRAINT `fertilizers_to_plants_fertilizers` FOREIGN KEY (`fertilizer_id`) REFERENCES `fertilizers`(`id`),
	CONSTRAINT `fertilizers_to_plants_plants` FOREIGN KEY (`plant_id`) REFERENCES `plants`(`id`)
);

CREATE TABLE IF NOT EXISTS `fertilizers_to_storages` (
	`id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
	`fertilizer_id` INT UNSIGNED NOT NULL,
	`storage_id` INT UNSIGNED NOT NULL,
	
	PRIMARY KEY (`id`),
	
	CONSTRAINT `fertilizers_to_storages_fertilizers` FOREIGN KEY (`fertilizer_id`) REFERENCES `fertilizers`(`id`),
	CONSTRAINT `fertilizers_to_storages_storages` FOREIGN KEY (`storage_id`) REFERENCES `storages`(`id`)
);
