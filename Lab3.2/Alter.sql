-- in case we're not using our database right now
use farmershelper;

-- Let's say we decided to change our naming and replace every occurence of 'plant' with 'crop'

-- Replacing 'plants' to 'crops' in every name
RENAME TABLE `plants` TO `crops`;
RENAME TABLE `plant_types` TO `crop_types`;
RENAME TABLE `plants_to_fields` TO `crops_to_fields`;
RENAME TABLE `plants_to_types` TO `crops_to_types`;
RENAME TABLE `fertilizers_to_plants` TO `fertilizers_to_crops`;

-- 'plants' are now 'crops' but only in table names
DESCRIBE `crops_to_types`;
-- still 'plant_id'

-- rename column to desired name
ALTER TABLE `crops_to_types` RENAME COLUMN `plant_id` TO `crop_id`;
DESCRIBE `crops_to_types`;

-- and so on for other tables
ALTER TABLE `fertilizers_to_crops` RENAME COLUMN `plant_id` TO `crop_id`;
ALTER TABLE `crops_to_fields` RENAME COLUMN `plant_id` TO `crpp_id`;

-- yet there are still traces of 'plants' in our DB 
SHOW CREATE TABLE `crops_to_types`;
-- both constraints are still referencing plants in their names

ALTER TABLE `crops_to_types`
	DROP CONSTRAINT `plants_to_types_plants`,
    ADD CONSTRAINT `crops_to_types_crops` FOREIGN KEY (`crop_id`) REFERENCES `crops` (`id`),
    DROP CONSTRAINT `plants_to_types_types`,
    ADD CONSTRAINT `crops_to_types_types` FOREIGN KEY (`type_id`) REFERENCES `crop_types` (`id`);
    
SHOW CREATE TABLE `crops_to_types`;
-- that's better

-- repeat for other tables 
ALTER TABLE `fertilizers_to_crops`
	DROP CONSTRAINT `fertilizers_to_plants_plants`,
	ADD CONSTRAINT `fertilizers_to_crops_crops` FOREIGN KEY (`crop_id`) REFERENCES `crops` (`id`),
    DROP CONSTRAINT `fertilizers_to_plants_fertilizers`,
	ADD CONSTRAINT `fertilizers_to_crops_fertilizers` FOREIGN KEY (`fertilizer_id`) REFERENCES `fertilizers` (`id`);

ALTER TABLE `crops_to_fields`
	DROP CONSTRAINT `plants_to_fields_plants`,
	ADD CONSTRAINT `crops_to_fields_crops` FOREIGN KEY (`crop_id`) REFERENCES `crops` (`id`),
    DROP CONSTRAINT `plants_to_fields_fields`,
	ADD CONSTRAINT `crops_to_fields_fields` FOREIGN KEY (`field_id`) REFERENCES `fields` (`id`);

-- last comand was not successful saying that 'crop_id' column does not exist

DESCRIBE `crops_to_fields`;
-- yes, 'crpp_id'. Doesn't seem right. Probably a mistake. Let's fix it
ALTER TABLE `crops_to_fields` RENAME COLUMN `crpp_id` TO `crop_id`;

-- now let's try again
ALTER TABLE `crops_to_fields`
	DROP CONSTRAINT `plants_to_fields_plants`,
	ADD CONSTRAINT `crops_to_fields_crops` FOREIGN KEY (`crop_id`) REFERENCES `crops` (`id`),
    DROP CONSTRAINT `plants_to_fields_fields`,
	ADD CONSTRAINT `crops_to_fields_fields` FOREIGN KEY (`field_id`) REFERENCES `fields` (`id`);
-- successfull.

-- now I see some problems in my DB planning. I forget to put volume for fertilizers relationships
-- let's add it

ALTER TABLE `fertilizers_to_storages`
	ADD COLUMN `volume` INT UNSIGNED NOT NULL;
    
ALTER TABLE `fields_to_fertilizers`
	ADD COLUMN `volume` INT UNSIGNED NOT NULL;

-- also noticed that missed UNSIGNED for volume of storage

ALTER TABLE `storages`
	MODIFY `volume` INT UNSIGNED NOT NULL;
-- fixed now

-- also there is no other way to tell fields apart than by their id-s. let's add number
ALTER TABLE `fields`
	ADD COLUMN `number` INT UNSIGNED NOT NULL;
    
-- same with storages
ALTER TABLE `storages`
	ADD COLUMN `number` INT UNSIGNED NOT NULL;
    



