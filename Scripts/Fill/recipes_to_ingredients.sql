INSERT INTO recipes_to_ingredients (recipe_id, item_id, amount)
VALUES ((SELECT id FROM recipes WHERE product_id=(SELECT id FROM items WHERE name='Work Bench')), (SELECT id FROM items WHERE name='Wood'), 10),
((SELECT id FROM recipes WHERE product_id=(SELECT id FROM items WHERE name='Furnace')), (SELECT id FROM items WHERE name='Wood'), 4),
((SELECT id FROM recipes WHERE product_id=(SELECT id FROM items WHERE name='Furnace')), (SELECT id FROM items WHERE name='Stone'), 20),
((SELECT id FROM recipes WHERE product_id=(SELECT id FROM items WHERE name='Furnace')), (SELECT id FROM items WHERE name='Torch'), 3),
((SELECT id FROM recipes WHERE product_id=(SELECT id FROM items WHERE name='Wooden Sword')), (SELECT id FROM items WHERE name='Wood'), 7),
((SELECT id FROM recipes WHERE product_id=(SELECT id FROM items WHERE name='Wooden Hammer')), (SELECT id FROM items WHERE name='Wood'), 8),
((SELECT id FROM recipes WHERE product_id=(SELECT id FROM items WHERE name='Copper Bar')), (SELECT id FROM items WHERE name='Copper Ore'), 3),
((SELECT id FROM recipes WHERE product_id=(SELECT id FROM items WHERE name='Copper Pickaxe')), (SELECT id FROM items WHERE name='Copper Bar'), 12),
((SELECT id FROM recipes WHERE product_id=(SELECT id FROM items WHERE name='Copper Pickaxe')), (SELECT id FROM items WHERE name='Wood'), 4),
((SELECT id FROM recipes WHERE product_id=(SELECT id FROM items WHERE name='Copper Axe')), (SELECT id FROM items WHERE name='Copper Bar'), 9),
((SELECT id FROM recipes WHERE product_id=(SELECT id FROM items WHERE name='Copper Axe')), (SELECT id FROM items WHERE name='Wood'), 3),
((SELECT id FROM recipes WHERE product_id=(SELECT id FROM items WHERE name='Copper Sword')), (SELECT id FROM items WHERE name='Copper Bar'), 8),
((SELECT id FROM recipes WHERE product_id=(SELECT id FROM items WHERE name='Copper Shortsword')), (SELECT id FROM items WHERE name='Copper Bar'), 7),
((SELECT id FROM recipes WHERE product_id=(SELECT id FROM items WHERE name='Torch')), (SELECT id FROM items WHERE name='Wood'), 1),
((SELECT id FROM recipes WHERE product_id=(SELECT id FROM items WHERE name='Torch')), (SELECT id FROM items WHERE name='Gel'), 1);
SELECT * FROM recipes_to_ingredients;