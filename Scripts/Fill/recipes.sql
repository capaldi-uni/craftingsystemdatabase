INSERT INTO recipes (product_id, product_amount)
VALUES ((SELECT id FROM items WHERE name='Work Bench'), 1),
((SELECT id FROM items WHERE name='Furnace'), 1),
((SELECT id FROM items WHERE name='Wooden Sword'), 1),
((SELECT id FROM items WHERE name='Wooden Hammer'), 1),
((SELECT id FROM items WHERE name='Copper Bar'), 1),
((SELECT id FROM items WHERE name='Copper Pickaxe'), 1),
((SELECT id FROM items WHERE name='Copper Axe'), 1),
((SELECT id FROM items WHERE name='Copper Sword'), 1),
((SELECT id FROM items WHERE name='Copper Shortsword'), 1),
((SELECT id FROM items WHERE name='Torch'), 3);
SELECT * FROM recipes;