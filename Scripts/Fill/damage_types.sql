INSERT INTO damage_types (name, defense_type_id)
VALUES ('Melee', (SELECT id FROM defense_types WHERE name='Melee')),
('Ranged', (SELECT id FROM defense_types WHERE name='Ranged')),
('Magic', (SELECT id FROM defense_types WHERE name='Magic')),
('Summon', (SELECT id FROM defense_types WHERE name='Summon')),
('Rogue', (SELECT id FROM defense_types WHERE name='Rogue')),
('Radiant', (SELECT id FROM defense_types WHERE name='Radiant')),
('True', NULL);
SELECT * FROM damage_types;