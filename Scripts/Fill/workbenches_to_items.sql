INSERT INTO workbenches_to_items (workbench_id, item_id)
VALUES ((SELECT id FROM workbenches WHERE name='Work Bench'), (SELECT id FROM items WHERE name='Work Bench')),
((SELECT id FROM workbenches WHERE name='Furnace'), (SELECT id FROM items WHERE name='Furnace')),
((SELECT id FROM workbenches WHERE name='Anvil'), (SELECT id FROM items WHERE name='Anvil'));
SELECT * FROM workbenches_to_items;