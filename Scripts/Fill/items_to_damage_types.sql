INSERT INTO items_to_damage_types (item_id, damage_type_id, value)
VALUES ((SELECT id FROM items WHERE name='Shield of Cthulhu'), (SELECT id FROM damage_types WHERE name='Melee'), 30),
((SELECT id FROM items WHERE name='Copper Sword'), (SELECT id FROM damage_types WHERE name='Melee'), 8),
((SELECT id FROM items WHERE name='Copper Shortsword'), (SELECT id FROM damage_types WHERE name='Melee'), 5),
((SELECT id FROM items WHERE name='Copper Pickaxe'), (SELECT id FROM damage_types WHERE name='Melee'), 4),
((SELECT id FROM items WHERE name='Copper Axe'), (SELECT id FROM damage_types WHERE name='Melee'), 3),
((SELECT id FROM items WHERE name='Wooden Sword'), (SELECT id FROM damage_types WHERE name='Melee'), 7),
((SELECT id FROM items WHERE name='Wooden Hammer'), (SELECT id FROM damage_types WHERE name='Melee'), 2),
((SELECT id FROM items WHERE name='Terra Blade'), (SELECT id FROM damage_types WHERE name='Melee'), 95);
SELECT * FROM items_to_damage_types;