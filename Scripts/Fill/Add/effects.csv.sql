INSERT INTO effects (name, description, image_id, effect_type_id)
VALUES ('Ironskin', 'Increase defense by 8', (SELECT id FROM images WHERE path='D:\\Images\\DB\\Ironskin.png'), (SELECT id FROM effect_types WHERE name='Buff')),
('Gravitation', 'Press UP to reverse gravity', (SELECT id FROM images WHERE path='D:\\Images\\DB\\Gravitation.png'), (SELECT id FROM effect_types WHERE name='Buff')),
('Holy Protection', 'You will dodge the next attack', (SELECT id FROM images WHERE path='D:\\Images\\DB\\Holy_Protection.png'), (SELECT id FROM effect_types WHERE name='Buff')),
('Poisoned', 'Slowly losing life', (SELECT id FROM images WHERE path='D:\\Images\\DB\\Poisoned.png'), (SELECT id FROM effect_types WHERE name='Debuff')),
('Chaos State', 'Using the Rod of Discord will take life', (SELECT id FROM images WHERE path='D:\\Images\\DB\\Chaos_State.png'), (SELECT id FROM effect_types WHERE name='Debuff'));
SELECT * FROM effects;