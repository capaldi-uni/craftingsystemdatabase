INSERT INTO items_to_effects (item_id, effect_id, duration)
VALUES ((SELECT id FROM items WHERE name='Ironskin Potion'), (SELECT id FROM effects WHERE name='Ironskin'), 480),
((SELECT id FROM items WHERE name='Gravitation Potion'), (SELECT id FROM effects WHERE name='Gravitation'), 180),
((SELECT id FROM items WHERE name='Rod of Discord'), (SELECT id FROM effects WHERE name='Chaos State'), 6);
SELECT * FROM items_to_effects;