INSERT INTO item_types (name, description)
VALUES ('Material', 'Used to make other items'),
('Weapon', 'Use it to attack enemies'),
('Armor', 'Wear it to get protection'),
('Accessory', 'Makes your cooler'),
('Instrument', 'Does some useful stuff'),
('Potion', 'Drink it to get buffs'),
('Block', 'Can be placed'),
('Furniture', 'NPC need it'),
('Quest item', 'You can give it to NPC to get reward'),
('Misc', 'May be it useful, may be not... Who knows?');
SELECT * FROM item_types;