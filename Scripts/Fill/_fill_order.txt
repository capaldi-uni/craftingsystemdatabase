Scripts on the same tier can be executed in any order;
Scripts on the next tier should be executed after all the scripts on every previous tier;

Order of execution:
	1. 	rarities
	1. 	item_types
	1. 	images
	1. 	stack_sizes
	1. 	character_classes
	1. 	effect_types
	1.	defense_types
	
		2.	damage_types
		2.	effects
		2.	workbenches
		2.	items
		
			3.	recipes
			3.	items_to_character_classes
			3.	items_to_damage_types     
			3.	items_to_defense_types    
			3.	items_to_effects
			3.	workbenches_to_items
			
				4.	recipes_to_ingredients
				4.	recipes_to_workbenches