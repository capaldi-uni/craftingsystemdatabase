INSERT INTO recipes_to_workbenches (recipe_id, workbench_id)
VALUES ((SELECT id FROM recipes WHERE product_id=(SELECT id FROM items WHERE name='Work Bench')), (SELECT id FROM workbenches WHERE name='By hand')),
((SELECT id FROM recipes WHERE product_id=(SELECT id FROM items WHERE name='Furnace')), (SELECT id FROM workbenches WHERE name='Work Bench')),
((SELECT id FROM recipes WHERE product_id=(SELECT id FROM items WHERE name='Wooden Sword')), (SELECT id FROM workbenches WHERE name='Work Bench')),
((SELECT id FROM recipes WHERE product_id=(SELECT id FROM items WHERE name='Wooden Hammer')), (SELECT id FROM workbenches WHERE name='Work Bench')),
((SELECT id FROM recipes WHERE product_id=(SELECT id FROM items WHERE name='Copper Bar')), (SELECT id FROM workbenches WHERE name='Furnace')),
((SELECT id FROM recipes WHERE product_id=(SELECT id FROM items WHERE name='Copper Pickaxe')), (SELECT id FROM workbenches WHERE name='Anvil')),
((SELECT id FROM recipes WHERE product_id=(SELECT id FROM items WHERE name='Copper Axe')), (SELECT id FROM workbenches WHERE name='Anvil')),
((SELECT id FROM recipes WHERE product_id=(SELECT id FROM items WHERE name='Copper Sword')), (SELECT id FROM workbenches WHERE name='Anvil')),
((SELECT id FROM recipes WHERE product_id=(SELECT id FROM items WHERE name='Copper Shortsword')), (SELECT id FROM workbenches WHERE name='Anvil')),
((SELECT id FROM recipes WHERE product_id=(SELECT id FROM items WHERE name='Torch')), (SELECT id FROM workbenches WHERE name='By hand'));
SELECT * FROM recipes_to_workbenches;