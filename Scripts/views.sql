-- Agregative view. Собирает информацию о предметах из нескольких таблиц
DROP VIEW IF EXISTS detailed_items;
CREATE VIEW detailed_items AS 
SELECT 
	items.name AS Item, 
    items.description AS Description,
    item_types.name AS Type,
    character_classes.name AS Class,
    rarities.name AS Rarity,
    items_to_damage_types.value AS Damage,
    damage_types.name AS `Damage type`,
    items_to_defense_types.value AS Defense,
    defense_types.name AS `Defense type`,
    items.base_price AS Price,
    items.stack_size AS Stack
FROM
		items JOIN item_types ON item_type_id = item_types.id
			JOIN rarities ON rarity_id = rarities.id
				LEFT JOIN items_to_character_classes ON items.id = items_to_character_classes.item_id
					LEFT JOIN character_classes ON class_id = character_classes.id
						LEFT JOIN items_to_defense_types ON items.id = items_to_defense_types.item_id
							LEFT JOIN defense_types ON defense_type_id = defense_types.id
								LEFT JOIN items_to_damage_types ON items.id = items_to_damage_types.item_id
									LEFT JOIN damage_types ON damage_type_id = damage_types.id
ORDER BY items.id;

-- Selective view. Выбирает только предметы относящиеся к типу c id 1
CREATE VIEW type_one AS
SELECT * FROM items WHERE item_type_id = 1;
