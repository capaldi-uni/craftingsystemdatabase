CREATE INDEX item_price ON items(base_price);

CREATE INDEX item_name ON items(name);

CREATE INDEX effect_name ON effects(name);

CREATE INDEX workbench_name ON workbenches(name);

CREATE INDEX rarity_name ON rarities(name);

CREATE INDEX effect_duation ON items_to_effects(duration);
