-- INSERT
# 18
INSERT INTO stack_sizes VALUES (10000), (20001);

# 19
INSERT INTO defense_types (name) VALUES ('True');

# 20
INSERT INTO images (path) VALUES
	('D:\\Images\\DB\\Ironskin.png'),
	('D:\\Images\\DB\\Ironskin_Potion.png'),
	('D:\\Images\\DB\\Gravitation.png'),
	('D:\\Images\\DB\\Gravitation_Potion.png'),
	('D:\\Images\\DB\\Holy_Protection.png'),
	('D:\\Images\\DB\\Poisoned.png'),
	('D:\\Images\\DB\\Chaos_State.png'),
	('D:\\Images\\DB\\Rod_of_Discord.png'),
	('Chaos_state.png');

# 21
INSERT INTO effects (name, description, image_id, effect_type_id) VALUES
	('Ironskin', 'Increase defense by 0', 
		(SELECT id FROM images WHERE path='D:\\Images\\DB\\Ironskin.png'), 
		(SELECT id FROM effect_types WHERE name='Buff')),
	('Gravitation', 'Press UP to reverse gravity', 
		(SELECT id FROM images WHERE path='D:\\Images\\DB\\Gravitation.png'), 
		(SELECT id FROM effect_types WHERE name='Buff')),
	('Holy Protection', 'You will dodge the next attack', 
		(SELECT id FROM images WHERE path='D:\\Images\\DB\\Holy_Protection.png'), 
		(SELECT id FROM effect_types WHERE name='Buff')),
	('Poisoned', 'Slowly losing life', 
		(SELECT id FROM images WHERE path='D:\\Images\\DB\\Poisoned.png'), 
		(SELECT id FROM effect_types WHERE name='Debuff')),
	('Chaos State', 'Using the Rod of Discord will take life', 
		(SELECT id FROM images WHERE path='D:\\Images\\DB\\Chaos_State.png'), 
		(SELECT id FROM effect_types WHERE name='Debuff')),
	('Chaos State1', 'Using the Rod of Discord will take life', 
		(SELECT id FROM images WHERE path='D:\\Images\\DB\\Chaos_State.png'), 
		(SELECT id FROM effect_types WHERE name='Debuff'));

# 22
INSERT INTO items (name, description, rarity_id, item_type_id, base_price, stack_size, image_id) VALUES 
	('Ironskin Potion', 'Increase defense by 8', 
		(SELECT id FROM rarities WHERE name='Blue'), 
		(SELECT id FROM item_types WHERE name='Potion'), 200, 30, 
		(SELECT id FROM images WHERE path='D:\\Images\\DB\\Ironskin_Potion.png')),
	('Gravitation Potion', 'Allows the control of gravity', 
		(SELECT id FROM rarities WHERE name='Blue'), 
		(SELECT id FROM item_types WHERE name='Potion'), 200, 30, 
		(SELECT id FROM images WHERE path='D:\\Images\\DB\\Gravitation_Potion.png')),
	('Rod of Discord', 'Teleports you to the position of the mouse', 
		(SELECT id FROM rarities WHERE name='Lime'), 
		(SELECT id FROM item_types WHERE name='Instrument'), 100000, 1, 
		(SELECT id FROM images WHERE path='D:\\Images\\DB\\Rod_of_Discord.png'));

# 23
INSERT INTO items_to_effects (item_id, effect_id, duration) VALUES 
	((SELECT id FROM items WHERE name='Ironskin Potion'), (SELECT id FROM effects WHERE name='Ironskin'), 480),
	((SELECT id FROM items WHERE name='Gravitation Potion'), (SELECT id FROM effects WHERE name='Gravitation'), 180),
	((SELECT id FROM items WHERE name='Rod of Discord'), (SELECT id FROM effects WHERE name='Chaos State'), 6);


-- UPDATE with WHERE	-- I use LIMIT 1 to update row without referencing primary key in where while in safe update/delete mode
# 24
UPDATE damage_types SET defense_type_id=(SELECT id FROM defense_types WHERE name='True') WHERE name='True' LIMIT 1;

# 25
UPDATE effects SET description='Increase defense by 8' WHERE name ='Ironskin' LIMIT 1;

# 26
UPDATE item_types SET name='Tool' WHERE name='Instrument' LIMIT 1;

# 27
UPDATE items SET description='Can be placed' WHERE id=1 OR id=2;

# 28
UPDATE stack_sizes SET value=20000 WHERE value=20001;

# 29
UPDATE damage_types SET defense_type_id=NULL WHERE name='True' LIMIT 1;


-- DELETE with WHERE
# 30
DELETE FROM stack_sizes WHERE value=20000;

# 31
DELETE FROM defense_types WHERE name='True' LIMIT 1;

# 32
DELETE FROM stack_sizes WHERE value=10000;

# 33
DELETE FROM effects WHERE name='Chaos State1' LIMIT 1;

# 34
DELETE FROM images WHERE path='Chaos_state.png' LIMIT 1;


-- SELECT, DISTINCT, WHERE, AND/OR/NOT, IN, BETWEEN, IS NULL, AS
# 35
SELECT DISTINCT base_price FROM items;

# 36
SELECT id, name FROM workbenches WHERE image_id IS NULL;

# 37
SELECT id, name, description, rarity_id FROM items WHERE rarity_id BETWEEN 1 AND 5;

# 38
SELECT id, name, description, stack_size FROM items WHERE stack_size IN (9, 99, 999);

# 39
SELECT name AS `Название`, description AS `Описание` FROM items;

# 40
SELECT * FROM items_to_character_classes WHERE item_id > 10 AND NOT class_id=5;

# 41
SELECT id, name, description FROM items WHERE name LIKE 'Copper%' OR name LIKE 'Wooden%';

# 42
SELECT name, color FROM rarities WHERE name NOT IN ('Blue', 'White', 'Gray', 'Green');

# 43
SELECT * FROM recipes WHERE product_amount>1;

# 44
SELECT name FROM items WHERE id IN (SELECT item_id FROM items_to_effects WHERE duration BETWEEN 100 AND 500);

# 45
SELECT * FROM items WHERE rarity_id=3 OR item_type_id=8 OR stack_size=1 AND base_price NOT BETWEEN 20 AND 100;

# 46
SELECT DISTINCT item_type_id FROM items;

# 47 
SELECT name FROM item_types WHERE id IN (SELECT DISTINCT item_type_id FROM items);

# 48
SELECT name FROM effects WHERE effect_type_id=2 AND id IN (SELECT effect_id FROM items_to_effects);

# 49
SELECT path AS not_png FROM images WHERE path NOT LIKE '%.png';

# 50
SELECT name FROM effect_types WHERE id IN (SELECT effect_type_id FROM effects);

# 51
SELECT * FROM items_to_damage_types WHERE value BETWEEN 5 AND 50; 

# 52
SELECT * FROM recipes_to_ingredients WHERE NOT (item_id=1 AND amount>1);

# 53
SELECT name FROM items WHERE id IN (SELECT product_id FROM recipes) AND name NOT LIKE 'Wooden%';

# 54
SELECT name FROM workbenches WHERE id<3 AND image_id IS NOT NULL;

# 55
SELECT name FROM character_classes WHERE id NOT IN (SELECT DISTINCT class_id FROM items_to_character_classes);

# 56 
SELECT description FROM effects WHERE effect_type_id=1 AND image_id>22;

# 57
SELECT DISTINCT item_id FROM recipes_to_ingredients;


-- LIKE
# 58
SELECT path AS potions FROM images WHERE path LIKE '%Potion%';

# 59
SELECT name FROM items WHERE name NOT LIKE 'Wood%' AND name NOT LIKE '%Potion';

# 60
SELECT color, name FROM rarities WHERE name LIKE 'Gr_y';

# 61
SELECT name FROM items WHERE name LIKE '%a%' AND name NOT LIKE '%b%';

# 62
SELECT name FROM character_classes WHERE name LIKE "W%";


-- COUNT, MAX, MIN, SUM, AVG
# 63
SELECT COUNT(*) FROM items;

# 64
SELECT MAX(value) FROM items_to_damage_types;

# 65
SELECT AVG(value) FROM items_to_damage_types WHERE value < 80;

# 66
SELECT SUM(amount * base_price) AS recipe_price FROM
		items
	JOIN
		recipes_to_ingredients
	ON items.id=item_id
WHERE recipe_id=(SELECT id FROM recipes WHERE product_id=(SELECT id FROM items WHERE name='pharetra') LIMIT 1);

# 67
SELECT COUNT(DISTINCT product_id) AS craftable_count FROM recipes;


-- GROUP BY, HAVING
# 68
SELECT name, description, average_price FROM
	(SELECT AVG(base_price) AS average_price, item_type_id FROM items GROUP BY item_type_id) AS t
JOIN
	item_types
ON item_type_id = item_types.id; 

# 69
SELECT name, average_value FROM
	(SELECT AVG(value) AS average_value, damage_type_id FROM items_to_damage_types GROUP BY damage_type_id) AS t
JOIN
	damage_types
ON damage_type_id = damage_types.id;

# 70
SELECT name, use_count FROM
	(SELECT COUNT(*) AS use_count, workbench_id FROM recipes_to_workbenches GROUP BY workbench_id) AS t
JOIN
	workbenches
ON workbench_id = workbenches.id;

# 71
SELECT stack_size, COUNT(*) AS item_count FROM items GROUP BY stack_size HAVING item_count > 5;

#72
SELECT recipe_id, SUM(amount) AS single_amount FROM recipes_to_ingredients GROUP BY recipe_id HAVING COUNT(*) = 1;


-- ORDER BY, ASC|DESC
# 73
SELECT name AS `Название`, description AS `Описание` FROM items ORDER BY `Название`;

# 74
SELECT name, color FROM rarities ORDER BY name DESC;

# 75
SELECT name, stack_size FROM items ORDER BY stack_size, name;

# 76
SELECT 
	items.name AS i_name, items.description AS i_description,
	item_types.name AS t_name, item_types.description AS t_description
FROM
	items JOIN item_types ON item_type_id = item_types.id
ORDER BY item_type_id, i_name;

# 77
SELECT * FROM items ORDER BY base_price DESC;


-- Nested SELECT's
# 78
SELECT name FROM items WHERE id IN 
	(SELECT product_id FROM recipes WHERE id IN 
		(SELECT recipe_id FROM recipes_to_workbenches WHERE workbench_id IN
			(SELECT id FROM workbenches WHERE name='Anvil')));
# 79
SELECT * FROM stack_sizes WHERE value IN 
	(SELECT stack_size FROM items WHERE id IN 
		(SELECT item_id FROM items_to_character_classes));


-- SELECT INTO
# 80
SELECT name, description INTO strings_only;	# not working

CREATE TABLE strings_only		# MySQL alternative for SELECT INTO - CREATE SELECT 
	SELECT name, description FROM items;


-- INSERT SELECT
# 81
INSERT INTO strings_only (SELECT name, description FROM effects);


-- UNION, EXCEPT, INTERSECT
# 82
SELECT name, description FROM
	(SELECT name, description FROM items) AS t1
UNION
	(SELECT name, description FROM item_types)
UNION
	(SELECT name, description FROM effects)
ORDER BY name;

# 83 -- EXCEPT a la MySQL
-- SELECT id, name FROM character_classes
-- EXCEPT
-- SELECT class_id FROM items_to_character_classes
SELECT id, name FROM character_classes
WHERE id NOT IN 
(SELECT class_id FROM items_to_character_classes);

# 84 -- INTERSECT a la MySQL
-- SELECT item_id FROM items_to_damage_types
-- INTERSECT
-- SELECT item_id FROM items_to_defense_types
SELECT item_id FROM items_to_damage_types
WHERE item_id IN
(SELECT item_id FROM items_to_defense_types);


-- JOIN
# 85
SELECT items.name AS Name, damage_types.name AS `Damage type` FROM
	items JOIN items_to_damage_types ON id = item_id
		JOIN damage_types ON damage_types.id = damage_type_id;

# 86
SELECT items.name AS Name, defense_types.name AS `Defense type` FROM
	items JOIN items_to_defense_types ON id = item_id
		JOIN defense_types ON defense_types.id = defense_type_id;

# 87
SELECT items.name AS Name, item_types.name AS Type, effects.name AS Effect, duration AS Duration FROM
	items JOIN items_to_effects ON id = item_id
		JOIN effects ON effect_id = effects.id
			JOIN item_types ON item_type_id = item_types.id;

# 88
SELECT items.name AS Item, damage_types.name AS Damage, character_classes.name AS Class FROM
	items_to_damage_types NATURAL JOIN items_to_character_classes
		JOIN damage_types ON damage_type_id = damage_types.id
			JOIN character_classes ON class_id = character_classes.id
				JOIN items ON item_id = items.id;

# 89
SELECT name AS Item, rarity_name AS Rarity, color AS Color FROM
	items NATURAL JOIN (SELECT id AS rarity_id, color, name AS rarity_name FROM rarities) AS rnm;

# 90
SELECT items.name AS Item, workbenches.name AS Workbench, product_amount AS Amount FROM 
	items JOIN recipes ON items.id = product_id
		JOIN recipes_to_workbenches ON recipes.id = recipe_id
			JOIN workbenches ON workbenches.id = workbench_id
ORDER BY Item;

# 91
SELECT id, name, effect_id, duration FROM
	items
LEFT JOIN 
	items_to_effects
ON items.id = item_id
ORDER BY effect_id DESC;

# 92
SELECT id, name, damage_type_id FROM
	items_to_damage_types
RIGHT JOIN
	items
ON items.id = item_id;

# 93 -- FULL OUTER JOIN a la MySQL
SELECT * FROM items_to_effects LEFT JOIN items_to_character_classes ON items_to_effects.item_id = items_to_character_classes.item_id
UNION
SELECT * FROM items_to_effects RIGHT JOIN items_to_character_classes ON items_to_effects.item_id = items_to_character_classes.item_id;

# 94 
SELECT workbenches.name AS Workbench, items.name AS Item FROM
	workbenches JOIN workbenches_to_items ON workbenches.id = workbench_id
		JOIN items ON items.id = item_id;

# 95	-- Nonsense join
SELECT * FROM
	rarities JOIN stack_sizes
ON id < 10 AND value >= 99;

# 96
SELECT COUNT(*) FROM rarities CROSS JOIN effects;

# 97
SELECT * FROM images CROSS JOIN rarities;


-- LIMIT
# 98 
SELECT * FROM images LIMIT 7;

# 99
SELECT * FROM strings_only ORDER BY name LIMIT 10, 10;

# 100
SELECT name FROM items ORDER BY stack_size DESC LIMIT 1;
