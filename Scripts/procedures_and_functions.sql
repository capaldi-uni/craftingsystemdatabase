DELIMITER $$
USE crafting_db$$

-- Поиск рецептов для заданного верстака
DROP PROCEDURE IF EXISTS find_recipes_on$$
CREATE PROCEDURE find_recipes_on(IN workbench VARCHAR(255))
DETERMINISTIC
READS SQL DATA
BEGIN

	SELECT id AS recipe_id, product_id FROM
	(
		recipes
	JOIN
		(
		SELECT recipe_id FROM recipes_to_workbenches
		WHERE workbench_id = (SELECT id FROM workbenches WHERE name = workbench)
		) AS rtow
	ON recipes.id = rtow.recipe_id
	);

END $$

CALL find_recipes_on('amet eros')$$


-- Поиск предметов имеющих как урон, так и защиту
DROP PROCEDURE IF EXISTS dmg_and_def$$
CREATE PROCEDURE dmg_and_def()
DETERMINISTIC
READS SQL DATA
BEGIN
	SELECT item_id FROM items_to_damage_types
	WHERE item_id IN
	(SELECT item_id FROM items_to_defense_types);
END$$

CALL dmg_and_def()$$


-- Поиск эффектов, связанных с предметом
DROP PROCEDURE IF EXISTS item_effects$$
CREATE PROCEDURE item_effects(IN item VARCHAR(255))
DETERMINISTIC
READS SQL DATA
BEGIN
	SELECT items.name AS Name, item_types.name AS Type, effects.name AS Effect, duration AS Duration FROM
	items JOIN items_to_effects ON id = item_id
		JOIN effects ON effect_id = effects.id
			JOIN item_types ON item_type_id = item_types.id
	WHERE items.name = item;
END$$

CALL item_effects('fusce')$$


-- Считает стоимость рецепта заданного предмета		-- GROUP BY recipe_id, MAX(*)
DROP FUNCTION IF EXISTS recipe_sum$$
CREATE FUNCTION recipe_sum(item VARCHAR(255))
RETURNS INTEGER
DETERMINISTIC
READS SQL DATA
BEGIN
	DECLARE total BIGINT UNSIGNED DEFAULT 0;
	SELECT SUM(amount * base_price) INTO total FROM
			items
		JOIN
			recipes_to_ingredients
		ON items.id=item_id
	WHERE recipe_id=(SELECT id FROM recipes WHERE product_id=(SELECT id FROM items WHERE name=item LIMIT 1) LIMIT 1);
    RETURN total;
END$$

SELECT recipe_sum('congue diam id')$$


-- Определяет выгоден ли предмет
DROP FUNCTION IF EXISTS is_profitable$$
CREATE FUNCTION is_profitable(item VARCHAR(255))
RETURNS INTEGER
DETERMINISTIC
READS SQL DATA
BEGIN
	DECLARE cost BIGINT DEFAULT (SELECT base_price FROM items WHERE name=item LIMIT 1);
    IF cost > recipe_sum(item) THEN
		RETURN TRUE;
	ELSE
		RETURN FALSE;
	END IF;
END$$

SELECT is_profitable('congue diam id')$$


-- Определяет складывается ли предмет в стопки
DROP FUNCTION IF EXISTS is_stackable$$
CREATE FUNCTION is_stackable(item VARCHAR(255))
RETURNS VARCHAR(6)
DETERMINISTIC
READS SQL DATA
BEGIN
	DECLARE stack INT DEFAULT (SELECT stack_size FROM items WHERE name=item LIMIT 1);
	IF stack = 1 THEN
		RETURN 'No';
	ELSEIF stack < 10 THEN
		RETURN 'Hardly';
	ELSE
		RETURN 'Yes';
	END IF;
END$$

SELECT is_stackable('turpis nec euismod')$$

DELIMITER ;
