-- Для заданного класса вывести топ 5 самых сильных по урону предметов.

SELECT items.name AS Item, value AS Damage FROM
	-- соединение таблицы предметов с классами и уроном
	character_classes
		JOIN items_to_character_classes ON items_to_character_classes.class_id = character_classes.id
			JOIN items ON items_to_character_classes.item_id = items.id
				JOIN items_to_damage_types ON items_to_damage_types.item_id = items.id
WHERE character_classes.name = 'mauris'	-- выбор нужного класса
ORDER BY Damage DESC	-- сортировка по урону от большего к меньшему
LIMIT 5;	-- выбор первых пяти


-- Вывести предметы и их редкость, у которых есть эффекты длительностью от % до % и название эффекта

SELECT items.name AS Item, rarities.name AS Rarity, effects.name AS Effect, items_to_effects.duration AS Duration FROM
	-- соединение предметов с с эффектами (названием и длительностью) и редкостью
	items 
		JOIN rarities ON items.rarity_id = rarities.id
			JOIN items_to_effects ON items.id = items_to_effects.item_id
				JOIN effects ON items_to_effects.effect_id = effects.id
WHERE Duration BETWEEN 100 AND 300;		-- ограничение промежутка длительности


-- Максимальное количество ингредиентов в рецепте по типам
SELECT item_types.name AS `Type`, MAX(i_count) AS `Maximum count` -- выбрать максимальное значение
FROM

	-- получить количество предметов по рецептам
	(SELECT product_id, COUNT(item_id) AS i_count FROM 
		recipes JOIN recipes_to_ingredients ON recipes.id = recipe_id
			GROUP BY recipe_id) AS ingredient_count
	
    -- через таблицу предметов соединить с таблицей типов
	JOIN items ON product_id = items.id
		JOIN item_types ON item_type_id = item_types.id
GROUP BY `Type`		-- группировка по типам предметов
ORDER BY `Maximum count` DESC;	
