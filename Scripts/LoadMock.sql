SET GLOBAL local_infile=true;


-- 1 character_classes
LOAD DATA LOCAL INFILE 'D:\\Projects\\Python\\DB\\MockData\\character_classes.csv'
INTO TABLE character_classes
FIELDS TERMINATED BY ','
ESCAPED BY '\\'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(id,name);


-- 2 defense_types
LOAD DATA LOCAL INFILE 'D:\\Projects\\Python\\DB\\MockData\\defense_types.csv'
INTO TABLE defense_types
FIELDS TERMINATED BY ','
ESCAPED BY '\\'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(id,name);


-- 3 damage_types
LOAD DATA LOCAL INFILE 'D:\\Projects\\Python\\DB\\MockData\\damage_types.csv'
INTO TABLE damage_types
FIELDS TERMINATED BY ','
ESCAPED BY '\\'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(id,name,defense_type_id);


-- 4 effect_types
LOAD DATA LOCAL INFILE 'D:\\Projects\\Python\\DB\\MockData\\effect_types.csv'
INTO TABLE effect_types
FIELDS TERMINATED BY ','
ESCAPED BY '\\'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(id,name);


-- 5 images
LOAD DATA LOCAL INFILE 'D:\\Projects\\Python\\DB\\MockData\\images.csv'
INTO TABLE images
FIELDS TERMINATED BY ','
ESCAPED BY '\\'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(id,path);


-- 6 effects
LOAD DATA LOCAL INFILE 'D:\\Projects\\Python\\DB\\MockData\\effects.csv'
INTO TABLE effects
FIELDS TERMINATED BY ','
ESCAPED BY '\\'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(id,name,description,image_id,effect_type_id);


-- 7 item_types
LOAD DATA LOCAL INFILE 'D:\\Projects\\Python\\DB\\MockData\\item_types.csv'
INTO TABLE item_types
FIELDS TERMINATED BY ','
ESCAPED BY '\\'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(id,name,description);


-- 8 stack_sizes
LOAD DATA LOCAL INFILE 'D:\\Projects\\Python\\DB\\MockData\\stack_sizes.csv'
INTO TABLE stack_sizes
FIELDS TERMINATED BY ','
ESCAPED BY '\\'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(value);


-- 9 rarities
LOAD DATA LOCAL INFILE 'D:\\Projects\\Python\\DB\\MockData\\rarities.csv'
INTO TABLE rarities
FIELDS TERMINATED BY ','
ESCAPED BY '\\'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(id,color,name);


-- 10 items
LOAD DATA LOCAL INFILE 'D:\\Projects\\Python\\DB\\MockData\\items.csv'
INTO TABLE items
FIELDS TERMINATED BY ','
ESCAPED BY '\\'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(id,name,description,image_id,base_price,rarity_id,item_type_id,stack_size);

UPDATE items SET base_price = base_price / 100 LIMIT 1000;

-- 11 items_to_character_classes
LOAD DATA LOCAL INFILE 'D:\\Projects\\Python\\DB\\MockData\\items_to_character_classes.csv'
INTO TABLE items_to_character_classes
FIELDS TERMINATED BY ','
ESCAPED BY '\\'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(item_id,class_id);


-- 12 items_to_damage_types
LOAD DATA LOCAL INFILE 'D:\\Projects\\Python\\DB\\MockData\\items_to_damage_types.csv'
INTO TABLE items_to_damage_types
FIELDS TERMINATED BY ','
ESCAPED BY '\\'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(item_id,damage_type_id,value);


-- 13 items_to_defense_types
LOAD DATA LOCAL INFILE 'D:\\Projects\\Python\\DB\\MockData\\items_to_defense_types.csv'
INTO TABLE items_to_defense_types
FIELDS TERMINATED BY ','
ESCAPED BY '\\'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(item_id,defense_type_id,value);


-- 14 items_to_effects
LOAD DATA LOCAL INFILE 'D:\\Projects\\Python\\DB\\MockData\\items_to_effects.csv'
INTO TABLE items_to_effects
FIELDS TERMINATED BY ','
ESCAPED BY '\\'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(item_id,effect_id,duration);


-- 15 recipes
LOAD DATA LOCAL INFILE 'D:\\Projects\\Python\\DB\\MockData\\recipes.csv'
INTO TABLE recipes
FIELDS TERMINATED BY ','
ESCAPED BY '\\'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(id,product_id,product_amount);


-- 16 workbenches
LOAD DATA LOCAL INFILE 'D:\\Projects\\Python\\DB\\MockData\\workbenches.csv'
INTO TABLE workbenches
FIELDS TERMINATED BY ','
ESCAPED BY '\\'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(id,name,image_id);


-- 17 recipes_to_ingredients
LOAD DATA LOCAL INFILE 'D:\\Projects\\Python\\DB\\MockData\\recipes_to_ingredients.csv'
INTO TABLE recipes_to_ingredients
FIELDS TERMINATED BY ','
ESCAPED BY '\\'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(id,recipe_id,item_id,amount);


-- 18 recipes_to_workbenches
LOAD DATA LOCAL INFILE 'D:\\Projects\\Python\\DB\\MockData\\recipes_to_workbenches.csv'
INTO TABLE recipes_to_workbenches
FIELDS TERMINATED BY ','
ESCAPED BY '\\'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(recipe_id,workbench_id);


-- 19 workbenches_to_items
LOAD DATA LOCAL INFILE 'D:\\Projects\\Python\\DB\\MockData\\workbenches_to_items.csv'
INTO TABLE workbenches_to_items
FIELDS TERMINATED BY ','
ESCAPED BY '\\'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(workbench_id,item_id);
