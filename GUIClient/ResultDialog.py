from PyQt5.QtWidgets import QDialog
from PyQt5.Qt import Qt
from ResultDialogUi import Ui_ResultDialog


class ResultDialog(QDialog, Ui_ResultDialog):
    def __init__(self, parent=None, items=()):
        super(ResultDialog, self).__init__(parent)
        self.setupUi(self)

        for item in items:
            self.list_widget.addItem(str(item))

        self.setWindowModality(Qt.ApplicationModal)
        self.show()
