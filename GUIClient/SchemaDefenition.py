from sqlalchemy.ext.automap import automap_base
from sqlalchemy.orm import relationship
from sqlalchemy import Column, ForeignKey, Integer, VARCHAR, Text


Base = automap_base()


class CharacterClass(Base):
    __tablename__ = 'character_classes'

    id = Column('id', Integer, primary_key=True)
    name = Column('name', VARCHAR)

    items = relationship('ItemClass', back_populates='character_class')

    def __repr__(self):
        return f'CharacterClass({self.id}: {self.name})'

    def __str__(self):
        return f'{self.name}'


class DamageType(Base):
    __tablename__ = 'damage_types'

    id = Column('id', Integer, primary_key=True)
    name = Column('name', VARCHAR)

    defense_type_id = Column('defense_type_id', Integer, ForeignKey('defense_types.id', ondelete="SET NULL"), unique=True)

    defense_type = relationship('DefenseType', back_populates='damage_type')
    applied_to = relationship('Damage', back_populates='type')

    def __repr__(self):
        return f'DamageType({self.id}: {self.name})'

    def __str__(self):
        return f'{self.name}'


class DefenseType(Base):
    __tablename__ = 'defense_types'

    id = Column('id', Integer, primary_key=True)
    name = Column('name', VARCHAR)

    damage_type = relationship('DamageType', back_populates='defense_type', uselist=False)
    applied_to = relationship('Defense', back_populates='type')

    def __repr__(self):
        return f'DefenseType({self.id}: {self.name})'

    def __str__(self):
        return f'{self.name}'


class EffectType(Base):
    __tablename__ = 'effect_types'

    id = Column('id', Integer, primary_key=True)
    name = Column('name', VARCHAR)

    effects = relationship('Effect', back_populates='type')

    def __repr__(self):
        return f'EffectType({self.id}: {self.name})'

    def __str__(self):
        return f'{self.name}'


class Effect(Base):
    __tablename__ = 'effects'

    id = Column('id', Integer, primary_key=True)
    name = Column('name', VARCHAR)
    description = Column('description', Text)

    image_id = Column('image_id', Integer, ForeignKey('images.id', ondelete="SET NULL"))
    effect_type_id = Column('effect_type_id', Integer, ForeignKey('effect_types.id', ondelete="RESTRICT"))

    type = relationship('EffectType', back_populates='effects')
    image = relationship('Image')

    applied_by = relationship('ItemEffect', back_populates='effect')

    def __repr__(self):
        return f'Effect({self.id}: {self.name})'

    def __str__(self):
        return f'{self.name}'


class Image(Base):
    __tablename__ = 'images'

    id = Column('id', Integer, primary_key=True)
    path = Column('path', VARCHAR)

    def __repr__(self):
        f_path = self.path.split('\\')[-1]
        return f'Image({self.id}: {f_path})'

    def __str__(self):
        return f'{self.path}'


class ItemType(Base):
    __tablename__ = 'item_types'

    id = Column('id', Integer, primary_key=True)
    name = Column('name', VARCHAR)
    description = Column('description', Text)

    items = relationship('Item', back_populates='type')

    def __repr__(self):
        return f'ItemType({self.id}: {self.name})'

    def __str__(self):
        return f'{self.name}'


class Item(Base):
    __tablename__ = 'items'

    id = Column('id', Integer, primary_key=True)
    name = Column('name', VARCHAR)
    description = Column('description', Text)
    base_price = Column('base_price', Integer)

    image_id = Column('image_id', Integer, ForeignKey('images.id', ondelete="SET NULL"))
    rarity_id = Column('rarity_id', Integer, ForeignKey('rarities.id', ondelete="RESTRICT"))
    item_type_id = Column('item_type_id', Integer, ForeignKey('item_types.id', ondelete="RESTRICT"))
    stack_size = Column('stack_size', Integer, ForeignKey('stack_sizes.value', ondelete="RESTRICT"))

    image = relationship('Image')
    rarity = relationship('Rarity', back_populates='items')
    type = relationship('ItemType', back_populates='items')
    stack = relationship('StackSize')

    classes = relationship('ItemClass', back_populates='item', cascade="save-update, merge, delete, delete-orphan")

    damages = relationship('Damage', back_populates='item', cascade="save-update, merge, delete, delete-orphan")
    defenses = relationship('Defense', back_populates='item', cascade="save-update, merge, delete, delete-orphan")

    applies = relationship('ItemEffect', back_populates='item', cascade="save-update, merge, delete, delete-orphan")

    recipes = relationship('Recipe', back_populates='product', cascade="save-update, merge, delete, delete-orphan")
    used_in = relationship('Ingredient', back_populates='item')
    used_as = relationship('WorkbenchItem', back_populates='item', cascade="save-update, merge, delete, delete-orphan")

    def __repr__(self):
        return f'Item({self.id}: {self.name})'

    def __str__(self):
        return f'{self.name}'


class ItemClass(Base):
    __tablename__ = 'items_to_character_classes'

    id = Column('id', Integer, primary_key=True)

    item_id = Column('item_id', Integer, ForeignKey('items.id', ondelete='CASCADE'))
    class_id = Column('class_id', Integer, ForeignKey('character_classes.id', ondelete='RESTRICT'))

    item = relationship('Item', back_populates='classes')
    character_class = relationship('CharacterClass', back_populates='items')

    def __repr__(self):
        return f'{self.item} is for {self.character_class}'

    def __str__(self):
        return f'{self.character_class.name}'


class Damage(Base):
    __tablename__ = 'items_to_damage_types'

    id = Column('id', Integer, primary_key=True)

    item_id = Column('item_id', Integer, ForeignKey('items.id', ondelete="CASCADE"))
    damage_type_id = Column('damage_type_id', Integer, ForeignKey('damage_types.id', ondelete="RESTRICT"))

    value = Column('value', Integer)

    item = relationship('Item', back_populates='damages')
    type = relationship('DamageType', back_populates='applied_to')

    def __repr__(self):
        return f'Damage({self.item.name}: {self.value} {self.type.name})'

    def __str__(self):
        return f'{self.value} of {self.type}'


class Defense(Base):
    __tablename__ = 'items_to_defense_types'

    id = Column('id', Integer, primary_key=True)

    item_id = Column('item_id', Integer, ForeignKey('items.id', ondelete="CASCADE"))
    defense_type_id = Column('defense_type_id', Integer, ForeignKey('defense_types.id', ondelete="RESTRICT"))

    value = Column('value', Integer)

    item = relationship('Item', back_populates='defenses')
    type = relationship('DefenseType', back_populates='applied_to')

    def __repr__(self):
        return f'Defense({self.item.name}: {self.value} {self.type.name})'

    def __str__(self):
        return f'{self.value} of {self.type}'


class ItemEffect(Base):
    __tablename__ = 'items_to_effects'

    id = Column('id', Integer, primary_key=True)

    item_id = Column('item_id', Integer, ForeignKey('items.id', ondelete="CASCADE"))
    effect_id = Column('effect_id', Integer, ForeignKey('effects.id', ondelete="RESTRICT"))

    duration = Column('duration', Integer)

    item = relationship('Item', back_populates='applies')
    effect = relationship('Effect', back_populates='applied_by')

    def __repr__(self):
        return f'ItemEffect({self.item.name}: {self.effect.name} for {self.duration} sec)'

    def __str__(self):
        return f'{self.effect} for {self.duration}'


class Rarity(Base):
    __tablename__ = 'rarities'

    id = Column('id', Integer, primary_key=True)
    color = Column('color', Integer)
    name = Column('name', VARCHAR)

    items = relationship('Item', back_populates='rarity')

    def __repr__(self):
        return f'Rarity({self.id}: {self.name})'

    def __str__(self):
        return f'{self.name}'


class Recipe(Base):
    __tablename__ = 'recipes'

    id = Column('id', Integer, primary_key=True)
    product_amount = Column('product_amount')

    product_id = Column('product_id', Integer, ForeignKey('items.id', ondelete="CASCADE"))

    product = relationship('Item', back_populates='recipes')
    uses = relationship('Ingredient', back_populates='recipe', cascade="save-update, merge, delete, delete-orphan")

    made_on = relationship('RecipeWorkbench', back_populates='recipe', cascade="save-update, merge, delete, delete-orphan")

    def __repr__(self):
        return f'Recipe({self.id}: {self.item})'

    def __str__(self):
        return f'{self.product_amount} of {self.product}'


class Ingredient(Base):
    __tablename__ = 'recipes_to_ingredients'

    id = Column('id', Integer, primary_key=True)

    recipe_id = Column('recipe_id', Integer, ForeignKey('recipes.id', ondelete="CASCADE"))
    item_id = Column('item_id', Integer, ForeignKey('items.id', ondelete="RESTRICT"))

    amount = Column('amount', Integer)

    recipe = relationship('Recipe', back_populates='uses')
    item = relationship('Item', back_populates='used_in')

    @property
    def name(self):
        return self.item.name

    def __repr__(self):
        return f'Ingredient({self.recipe.product.name} <- {self.item.name} × {self.amount})'

    def __str__(self):
        return f'{self.amount} of {self.name}'


class RecipeWorkbench(Base):
    __tablename__ = 'recipes_to_workbenches'

    id = Column('id', Integer, primary_key=True)

    workbench_id = Column('workbench_id', Integer, ForeignKey('workbenches.id', ondelete='RESTRICT'))
    recipe_id = Column('recipe_id', Integer, ForeignKey('recipes.id', ondelete='CASCADE'))

    workbench = relationship('Workbench', back_populates='recipes')
    recipe = relationship('Recipe', back_populates='made_on')

    @property
    def name(self):
        return self.workbench.name

    def __repr__(self):
        return f'{self.recipe} is made on {self.workbench}'

    def __str__(self):
        return f'{self.name}'


class StackSize(Base):
    __tablename__ = 'stack_sizes'

    value = Column('value', Integer, primary_key=True)

    def __repr__(self):
        return f'StackSize({self.value})'

    def __str__(self):
        return f'{self.value}'


class Workbench(Base):
    __tablename__ = 'workbenches'

    id = Column('id', Integer, primary_key=True)
    name = Column('name', VARCHAR)
    image_id = Column('image_id', Integer, ForeignKey('images.id', ondelete="SET NULL"))

    image = relationship('Image')

    recipes = relationship('RecipeWorkbench', back_populates='workbench')
    items = relationship('WorkbenchItem', back_populates='workbench', cascade="save-update, merge, delete, delete-orphan")

    def __repr__(self):
        return f'Workbench({self.id}: {self.name})'

    def __str__(self):
        return f'{self.name}'


class WorkbenchItem(Base):
    __tablename__ = 'workbenches_to_items'

    id = Column('id', Integer, primary_key=True)

    workbench_id = Column('workbench_id', Integer, ForeignKey('workbenches.id', ondelete='CASCADE'))
    item_id = Column('item_id', Integer, ForeignKey('items.id', ondelete='CASCADE'))

    workbench = relationship('Workbench', back_populates='items')
    item = relationship('Item', back_populates='used_as')

    def __repr__(self):
        return f'{self.item} is {self.workbench}'

    def __str__(self):
        return f'{self.workbench}'
