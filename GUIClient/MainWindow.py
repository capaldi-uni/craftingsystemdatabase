from PyQt5.QtWidgets import QMainWindow, QMessageBox, \
    QTableWidgetItem, QListWidgetItem, QTreeWidgetItem, QTreeWidgetItemIterator, \
    QListWidget, QTreeWidget, QComboBox, \
    QWidget, QMenu, QAction
from PyQt5.QtGui import QCloseEvent, QIcon
from PyQt5.QtCore import QPoint, pyqtSignal
from PyQt5.Qt import Qt

from MainWindowUi import Ui_MainWindow

from ResultDialog import ResultDialog

from Database import Database
import SchemaDefenition as Schema

from typing import Iterable, Tuple, Union, List, Optional

SELECT_PLACEHOLDER = 'Select table...'


# TODO: REPLACE ITEM BASED WIDGETS WITH MODEL BASED
class MainWindow(QMainWindow, Ui_MainWindow):
    DB = None

    item_deleted = pyqtSignal(Schema.Item)
    item_modified = pyqtSignal(Schema.Item)

    # table view
    current_table = None
    table_info = None
    table_columns = None
    table_contents = None

    current_page = 0
    records_per_page = 0

    # form edit
    tracked_item: Optional[Schema.Item] = None
    tracked_list_item: Optional[QListWidgetItem] = None
    tracked_recipe: Optional[Schema.Recipe] = None
    tracked_tree_item: Optional[QTreeWidgetItem] = None
    previous_recipe: Optional[Schema.Recipe] = None
    recipe_filter: Optional[Schema.Item] = None

    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)
        self.setupUi(self)

        self.icon = QIcon('res/icon.ico')
        self.setWindowIcon(self.icon)

        self.DB = Database(echo=False)

        self.init_table_view()
        self.init_form_edit()
        self.init_queries()

    def closeEvent(self, event: QCloseEvent) -> None:
        if self.DB.Tables.modified():
            button = QMessageBox.question(self, 'Save changes?', 'Do you want to save changes?',
                                          QMessageBox.Save | QMessageBox.Discard | QMessageBox.Cancel)

            if button == QMessageBox.Save:
                self.DB.Tables.apply()
            elif button == QMessageBox.Discard:
                self.DB.Tables.revert()
            else:
                event.ignore()
                return

        event.accept()

    # region table view -------
    def init_table_view(self):
        self.records_per_page = self.table_view_records_per_page.value()

        self.populate_table_selectors()

        self.table_view_selection.currentTextChanged.connect(self.on_table_changed)
        self.table_view_records_per_page.valueChanged.connect(self.on_records_per_page_changed)

        self.table_view_next.clicked.connect(self.on_next_page)
        self.table_view_previous.clicked.connect(self.on_previous_page)

        self.on_table_changed(SELECT_PLACEHOLDER)

    def populate_table_selectors(self):
        tables = self.DB.get_table_names()
        tables.insert(0, SELECT_PLACEHOLDER)

        for table in tables:
            self.table_view_selection.addItem(table)

    def on_table_changed(self, table_name):
        if table_name and table_name != SELECT_PLACEHOLDER:
            self.current_table = table_name
            self.table_info, self.table_columns = self.DB.get_table_info(table_name)

            self.current_page = 0
            self.display_table(table_name)
        else:
            self.current_table = None
            self.table_info = None
            self.table_columns = None
            self.table_contents = None

            self.table_view_table.setRowCount(0)
            self.table_view_table.setColumnCount(0)

        self.check_pages()

    def on_records_per_page_changed(self, new_value):
        self.records_per_page = new_value
        self.on_table_changed(self.current_table)

    def on_next_page(self):
        self.current_page += 1
        self.display_current_table()
        self.check_pages()

    def on_previous_page(self):
        self.current_page -= 1
        self.display_current_table()
        self.check_pages()

    def check_pages(self):
        if self.current_table:
            if self.current_page == 0:
                self.table_view_previous.setEnabled(False)
            else:
                self.table_view_previous.setEnabled(True)

            if (self.current_page + 1) * self.records_per_page >= self.table_info['row_count']:
                self.table_view_next.setEnabled(False)
            else:
                self.table_view_next.setEnabled(True)
        else:
            self.table_view_previous.setEnabled(False)
            self.table_view_next.setEnabled(False)

    def display_current_table(self):
        if self.current_table:
            self.display_table(self.current_table)

    def display_table(self, table_name):
        start_index = self.current_page * self.records_per_page

        self.table_contents = self.DB.fetch_from_table(table_name, self.records_per_page, start_index)

        actual_count = len(self.table_contents)
        column_count = len(self.table_columns)
        self.table_view_table.setRowCount(actual_count)
        self.table_view_table.setColumnCount(column_count)

        self.table_view_table.verticalHeader().hide()
        self.table_view_table.setHorizontalHeaderLabels(self.get_current_table_header())

        for field_number in range(column_count):
            field_name = self.table_columns[field_number]['Field']
            for row_number in range(actual_count):
                table_item = QTableWidgetItem(str(self.table_contents[row_number][field_name]))
                self.table_view_table.setItem(row_number, field_number, table_item)

        self.table_view_table.resizeColumnsToContents()

        self.table_view_page_info.setText('{}-{}/{}'.format(start_index + 1, start_index+actual_count, self.table_info['row_count']))

    def get_current_table_header(self):
        return [item['Field'] for item in self.table_columns]
    # -------------------------

    # region form edit --------
    def init_form_edit(self):
        self.actionSave.triggered.connect(self.on_menu_save)
        self.actionRevert.triggered.connect(self.on_menu_revert)

        self.item_connect()
        self.recipe_connect()

        self.item_set_enabled(False)
        self.recipe_set_enabled(False)

        if self.item_fill_lists():
            ...  # self.item_list_widget.setCurrentRow(0)

        self.recipe_fill_lists()

    # item methods ------------
    def current_item(self) -> Schema.Item:
        list_item = self.item_list_widget.currentItem()
        if list_item:
            return list_item.data(Qt.UserRole)

    def item_set_enabled(self, state):
        for widget in [self.item_name, self.item_description, self.item_type, self.item_rarity, self.item_stack_size,
                       self.item_price_copper, self.item_price_silver, self.item_price_gold, self.item_price_platinum,
                       self.item_find_recipes, self.item_find_usages]:
            widget.setEnabled(state)

    def item_fill_lists(self):
        self.item_list_widget.clear()
        self.item_type.clear()
        self.item_rarity.clear()

        items = self.DB.Tables.Items()

        for item in items:
            list_item = QListWidgetItem(item.name)
            list_item.setData(Qt.UserRole, item)
            self.item_list_widget.addItem(list_item)

            if self.tracked_item and self.tracked_item is item:
                self.tracked_list_item = list_item

        for _type in self.DB.Tables.Types():
            self.item_type.addItem(_type.name, _type)

        # self.item_type.addItem('Add...')

        for rarity in self.DB.Tables.Rarities():
            self.item_rarity.addItem(rarity.name, rarity)

        # self.item_rarity.addItem('Add...')

        for stack_size in self.DB.Tables.StackSizes():
            self.item_stack_size.addItem(str(stack_size.value), stack_size)

        # self.item_stack_size.addItem('Add...')  # ?

        return self.item_list_widget.count()

        # TODO: Add... for drop lists

    def item_update_lists(self):
        current = self.item_list_widget.currentRow()
        # TODO: replace index-based selection with data-based (in case of deletion of selected item)

        self.item_set_enabled(False)
        self.item_fill_lists()

        total_rows = self.item_list_widget.count()
        if current >= total_rows:
            current = total_rows - 1

        self.item_list_widget.setCurrentRow(current)

    def item_display(self, item: Schema.Item):
        self.item_name.setText(item.name)
        self.item_description.setText(item.description)

        self.select_entry_with_data(self.item_type, item.type)

        self.select_entry_with_data(self.item_rarity, item.rarity)

        self.select_entry_with_data(self.item_stack_size, item.stack)

        price = item.base_price
        self.item_price_copper.setValue(price % 100)

        price //= 100
        self.item_price_silver.setValue(price % 100)

        price //= 100
        self.item_price_gold.setValue(price % 100)

        price //= 100
        self.item_price_platinum.setValue(price)

    def item_new(self, list_item: QListWidgetItem = None):
        if not list_item:
            item = Schema.Item(name='New Item', description='', base_price=0)
            item.type = self.item_type.itemData(0, Qt.UserRole)
            item.rarity = self.item_rarity.itemData(0, Qt.UserRole)
            item.stack = self.item_stack_size.itemData(0, Qt.UserRole)

            self.DB.Tables.add(item)

            self.tracked_item = item

            self.item_set_enabled(False)
            self.item_fill_lists()

            self.item_list_widget.setCurrentItem(self.tracked_list_item)

            self.tracked_item = None
            self.tracked_list_item = None

            self.statusbar.showMessage('New item created', 5000)

        # TODO: copy item

    def item_delete(self, list_item: QListWidgetItem):
        item = list_item.data(Qt.UserRole)
        if self.DB.Tables.DeleteItem(item):
            self.item_list_widget.takeItem(self.item_list_widget.row(list_item))

            self.statusbar.showMessage('Item deleted', 5000)

            self.item_deleted.emit(item)
        else:
            self.statusbar.showMessage('Can not delete item', 5000)
    # -------------------------

    # item slots --------------
    def item_connect(self):
        self.item_list_widget.currentItemChanged.connect(self.on_item_selected)
        self.item_list_widget.customContextMenuRequested.connect(self.on_item_right_clicked)

        self.item_name.editingFinished.connect(self.on_item_name_changed)
        self.item_description.textChanged.connect(self.on_item_description_changed)

        self.item_type.currentIndexChanged.connect(self.on_item_type_changed)
        self.item_rarity.currentIndexChanged.connect(self.on_item_rarity_changed)
        self.item_stack_size.currentIndexChanged.connect(self.on_item_stack_size_changed)

        self.item_find_usages.clicked.connect(self.on_item_find_usages)
        self.item_find_recipes.clicked.connect(self.on_item_find_recipes)

        for widget in [self.item_price_copper, self.item_price_silver,
                       self.item_price_gold, self.item_price_platinum]:
            widget.editingFinished.connect(self.on_item_price_changed)

    def on_item_find_usages(self):
        self.recipe_set_filter(self.current_item(), uses=True)
        self.form_edit_tab_widget.setCurrentIndex(1)

    def on_item_find_recipes(self):
        self.recipe_set_filter(self.current_item(), creates=True)
        self.form_edit_tab_widget.setCurrentIndex(1)

    def on_item_selected(self, list_item: QListWidgetItem):
        self.item_set_enabled(False)
        if list_item:
            item = list_item.data(Qt.UserRole)
            if item:
                self.item_display(item)

            self.item_set_enabled(True)

    def on_item_right_clicked(self, point: QPoint):
        i_action, list_item = self.exec_item_widget_menu(self.item_list_widget, point)

        if i_action == 0:
            self.item_new()
        elif i_action == 1:
            self.item_delete(list_item)

    def on_item_name_changed(self):
        if self.item_name.isEnabled():
            item = self.current_item()
            if item:
                new_name = self.item_name.text()
                item.name = new_name

                self.item_list_widget.currentItem().setText(new_name)

                self.item_modified.emit(item)

    def on_item_description_changed(self):
        if self.item_description.isEnabled():
            item = self.current_item()
            if item:
                item.description = self.item_description.toPlainText()
                self.item_modified.emit(item)

    def on_item_type_changed(self):
        if self.item_type.isEnabled():
            item = self.current_item()
            if item:
                item.type = self.item_type.currentData()
                self.item_modified.emit(item)

    def on_item_rarity_changed(self):
        if self.item_rarity.isEnabled():
            item = self.current_item()
            if item:
                item.rarity = self.item_rarity.currentData()
                self.item_modified.emit(item)

    def on_item_stack_size_changed(self):
        if self.item_stack_size.isEnabled():
            item = self.current_item()
            if item:
                item.stack = self.item_stack_size.currentData()
                self.item_modified.emit(item)

    def on_item_price_changed(self):
        if any([self.item_price_copper.isEnabled(), self.item_price_silver.isEnabled(),
                self.item_price_gold.isEnabled(), self.item_price_platinum.isEnabled()]):
            item = self.current_item()
            if item:
                c = self.item_price_copper.value()
                s = self.item_price_silver.value()
                g = self.item_price_gold.value()
                p = self.item_price_platinum.value()

                item.base_price = c + 100 * (s + 100 * (g + 100 * p))
                self.item_modified.emit(item)
    # -------------------------

    # recipe methods ----------
    def current_recipe(self) -> Schema.Recipe:
        return self.current_top_level().data(0, Qt.UserRole)

    def current_ingredient(self) -> Schema.Ingredient:
        list_item = self.recipe_ingredient_list.currentItem()
        if list_item:
            return list_item.data(Qt.UserRole)

    def current_recipe_workbench(self) -> Schema.RecipeWorkbench:
        list_item = self.recipe_workbench_list.currentItem()
        if list_item:
            return list_item.data(Qt.UserRole)

    def current_top_level(self) -> QTreeWidgetItem:
        tree_item = self.recipe_tree_widget.currentItem()
        if tree_item:
            if data := tree_item.data(0, Qt.UserRole):
                if isinstance(data, Schema.Recipe):
                    return tree_item
                if isinstance(data, Schema.Ingredient) or isinstance(data, Schema.RecipeWorkbench):
                    return tree_item.parent().parent()
            else:
                return tree_item.parent()

    def recipe_set_enabled(self, state=True):
        self.recipe_product.setEnabled(state)
        self.recipe_product_amount.setEnabled(state)

        self.recipe_ingredient.setEnabled(state)
        self.recipe_ingredient_amount.setEnabled(state)
        self.recipe_ingredient_list.setEnabled(state)

        self.recipe_workbench.setEnabled(state)
        self.recipe_workbench_list.setEnabled(state)

    def recipe_fill_lists(self):
        self.recipe_fill_tree(self.recipe_get_filtered_list())

        self.recipe_workbench_list.clear()

        self.recipe_ingredient.clear()
        self.recipe_product.clear()

        for item in self.DB.Tables.Items():
            self.recipe_product.addItem(item.name, item)
            self.recipe_ingredient.addItem(item.name, item)

        for workbench in self.DB.Tables.Workbenches():
            self.recipe_workbench.addItem(workbench.name, workbench)

    def recipe_update_lists(self):
        folding = [False, False, False]

        tli = self.current_top_level()
        if tli:
            self.tracked_recipe = tli.data(0, Qt.UserRole)
            folding = [tli.isExpanded(), tli.child(0).isExpanded(), tli.child(1).isExpanded()]

        self.recipe_set_enabled(False)

        self.recipe_fill_lists()

        if self.tracked_tree_item:
            self.recipe_tree_widget.setCurrentItem(self.tracked_tree_item)
            self.tracked_tree_item.setExpanded(folding[0])
            self.tracked_tree_item.child(0).setExpanded(folding[1])
            self.tracked_tree_item.child(1).setExpanded(folding[2])

        self.tracked_recipe = None
        self.tracked_tree_item = None

    def recipe_fill_tree(self, recipes: Iterable[Schema.Recipe]):
        self.recipe_tree_widget.clear()
        for recipe in recipes:
            recipe_item = self.recipe_to_tree_item(recipe)
            self.recipe_tree_widget.addTopLevelItem(recipe_item)

            if self.tracked_recipe and self.tracked_recipe is recipe:
                self.tracked_tree_item = recipe_item

        self.recipe_tree_widget.resizeColumnToContents(0)

    @staticmethod
    def recipe_to_tree_item(recipe: Schema.Recipe):
        tree_item = QTreeWidgetItem()
        tree_item.setText(0, str(recipe))
        tree_item.setData(0, Qt.UserRole, recipe)

        ingredients_entry = QTreeWidgetItem(tree_item, ['Ingredients'])

        for ingredient in recipe.uses:
            ingredient_item = QTreeWidgetItem(ingredients_entry, [str(ingredient)])
            ingredient_item.setData(0, Qt.UserRole, ingredient)

        workbenches_entry = QTreeWidgetItem(tree_item, ['Workbenches'])

        for recipe_workbench in recipe.made_on:
            workbench_item = QTreeWidgetItem(workbenches_entry, [str(recipe_workbench)])
            workbench_item.setData(0, Qt.UserRole, recipe_workbench)

        return tree_item

    def recipe_display(self, recipe: Schema.Recipe):
        self.select_entry_with_data(self.recipe_product, recipe.product)
        self.recipe_product_amount.setValue(recipe.product_amount)

        self.recipe_ingredient_list.clear()
        self.recipe_workbench_list.clear()

        for ingredient in recipe.uses:
            list_item = QListWidgetItem(f'{ingredient.amount} of {ingredient.name}')
            list_item.setData(Qt.UserRole, ingredient)
            self.recipe_ingredient_list.addItem(list_item)

        for recipe_workbench in recipe.made_on:
            list_item = QListWidgetItem(f'{recipe_workbench.name}')
            list_item.setData(Qt.UserRole, recipe_workbench)
            self.recipe_workbench_list.addItem(list_item)

    def recipe_set_filter(self, item: Schema.Item, creates=False, uses=False):
        self.recipe_filter = None

        self.recipe_filter_checkbox_creates.setChecked(creates)
        self.recipe_filter_checkbox_uses.setChecked(uses)

        self.recipe_filter_label.setText(str(item))
        self.recipe_clear_filters.setEnabled(True)
        self.recipe_filter_checkbox_creates.setEnabled(True)
        self.recipe_filter_checkbox_uses.setEnabled(True)

        self.recipe_filter = item

        self.recipe_update_lists()

    def recipe_check_filter(self, item: Schema.Item):
        if item is self.recipe_filter:
            self.on_recipe_clear_filters()

    def recipe_get_filtered_list(self) -> List[Schema.Recipe]:
        if self.recipe_filter is None:
            return self.DB.Tables.Recipes()

        res_list = set()

        if self.recipe_filter_checkbox_creates.isChecked():
            res_list |= set(self.recipe_filter.recipes)

        if self.recipe_filter_checkbox_uses.isChecked():
            res_list |= set([ingredient.recipe for ingredient in self.recipe_filter.used_in])

        return sorted(list(res_list), key=lambda elem: elem.id)

    def recipe_new(self, base_recipe: Schema.Recipe = None):
        if not base_recipe:
            recipe = Schema.Recipe()
            recipe.product_amount = 1

            if self.recipe_filter:
                recipe.product = self.recipe_filter
            else:
                recipe.product = self.recipe_product.itemData(0, Qt.UserRole)

            self.DB.Tables.add(recipe)

            tree_item = self.recipe_to_tree_item(recipe)

            self.recipe_tree_widget.addTopLevelItem(tree_item)
            self.recipe_tree_widget.setCurrentItem(tree_item)

    def recipe_delete(self, recipe: Schema.Recipe):
        current = self.recipe_tree_widget.currentItem()
        if current.data(0, Qt.UserRole) is recipe:
            inv = self.recipe_tree_widget.invisibleRootItem()
            idx = inv.indexOfChild(current)

            if idx == inv.childCount() - 1:
                if idx != 0:
                    new_idx = idx - 1
                else:
                    new_idx = None
            else:
                new_idx = idx

            inv.takeChild(idx)

            if new_idx is not None:
                self.recipe_tree_widget.setCurrentItem(inv.child(new_idx))
        else:
            self.remove_by_data(self.recipe_tree_widget, recipe)

        self.DB.Tables.delete(recipe)

    def recipe_ingredient_new(self, base_ingredient: Schema.Ingredient = None):
        if not base_ingredient:
            tli = self.current_top_level()

            recipe = self.current_recipe()

            item = self.recipe_ingredient.itemData(0, Qt.UserRole)

            ingredient = Schema.Ingredient()

            ingredient.amount = 1
            ingredient.item = item
            ingredient.recipe = recipe

            self.DB.Tables.refresh(recipe)

            tree_item = QTreeWidgetItem([str(ingredient)])
            tree_item.setData(0, Qt.UserRole, ingredient)
            tli.child(0).addChild(tree_item)

            self.recipe_display(recipe)

            self.recipe_tree_widget.setCurrentItem(tree_item)

    def recipe_ingredient_delete(self, ingredient: Schema.Ingredient):
        new_index = self.recipe_ingredient_list.currentRow()

        if new_index == self.recipe_ingredient_list.count() - 1:
            new_index -= 1

        current_in_tree = self.recipe_tree_widget.currentItem().data(0, Qt.UserRole)
        top_level = self.current_top_level()

        select_top_level = False

        if ingredient is current_in_tree:
            select_top_level = True

        self.remove_by_data(self.recipe_tree_widget, ingredient)
        self.remove_by_data(self.recipe_ingredient_list, ingredient)

        recipe = ingredient.recipe

        self.DB.Tables.delete(ingredient)
        self.DB.Tables.refresh(recipe)

        if select_top_level:
            self.recipe_tree_widget.setCurrentItem(top_level)

        self.recipe_ingredient_list.setCurrentRow(new_index)

    def recipe_workbench_new(self, base_workbench: Schema.RecipeWorkbench = None):
        if not base_workbench:
            tli = self.current_top_level()

            recipe = self.current_recipe()
            workbench = self.recipe_workbench.itemData(0, Qt.UserRole)

            recipe_workbench = Schema.RecipeWorkbench()
            recipe_workbench.recipe = recipe
            recipe_workbench.workbench = workbench

            self.DB.Tables.refresh(recipe)

            tree_item = QTreeWidgetItem([str(recipe_workbench)])
            tree_item.setData(0, Qt.UserRole, recipe_workbench)
            tli.child(1).addChild(tree_item)

            self.recipe_display(recipe)

            self.recipe_tree_widget.setCurrentItem(tree_item)

    def recipe_workbench_delete(self, recipe_workbench: Schema.RecipeWorkbench):
        new_index = self.recipe_workbench_list.currentRow()

        if new_index == self.recipe_workbench_list.count() - 1:
            new_index -= 1

        current_in_tree = self.recipe_tree_widget.currentItem().data(0, Qt.UserRole)
        top_level = self.current_top_level()

        select_top_level = False

        if recipe_workbench is current_in_tree:
            select_top_level = True

        self.remove_by_data(self.recipe_tree_widget, recipe_workbench)
        self.remove_by_data(self.recipe_workbench_list, recipe_workbench)

        recipe = recipe_workbench.recipe

        self.DB.Tables.delete(recipe_workbench)
        self.DB.Tables.refresh(recipe)

        if select_top_level:
            self.recipe_tree_widget.setCurrentItem(top_level)

        self.recipe_workbench_list.setCurrentRow(new_index)
    # -------------------------

    # recipe slots ------------
    def recipe_connect(self):
        self.recipe_tree_widget.currentItemChanged.connect(self.on_recipe_tree_item_selected)

        self.recipe_ingredient_list.currentItemChanged.connect(self.on_recipe_ingredient_selected)
        self.recipe_workbench_list.currentItemChanged.connect(self.on_recipe_workbench_selected)

        self.recipe_product.currentIndexChanged.connect(self.on_recipe_product_changed)
        self.recipe_product_amount.valueChanged.connect(self.on_recipe_product_amount_changed)

        self.recipe_ingredient.currentIndexChanged.connect(self.on_recipe_ingredient_changed)
        self.recipe_ingredient_amount.valueChanged.connect(self.on_recipe_ingredient_amount_changed)

        self.recipe_workbench.currentIndexChanged.connect(self.on_recipe_workbench_changed)

        self.recipe_tree_widget.customContextMenuRequested.connect(self.on_recipe_tree_item_right_clicked)
        self.recipe_ingredient_list.customContextMenuRequested.connect(self.on_recipe_ingredient_right_clicked)
        self.recipe_workbench_list.customContextMenuRequested.connect(self.on_recipe_workbench_right_clicked)

        self.recipe_clear_filters.clicked.connect(self.on_recipe_clear_filters)
        self.recipe_filter_checkbox_creates.stateChanged.connect(self.on_recipe_filter_checked)
        self.recipe_filter_checkbox_uses.stateChanged.connect(self.on_recipe_filter_checked)

        self.item_modified.connect(self.on_item_modified)
        self.item_deleted.connect(self.on_item_deleted)

    def on_item_modified(self, item: Schema.Item):
        for widget in [self.recipe_product, self.recipe_ingredient]:
            self.update_text_by_data(widget, item)

        for recipe in item.recipes:
            self.update_text_by_data(self.recipe_tree_widget, recipe)

        for ingredient in item.used_in:
            self.update_text_by_data(self.recipe_tree_widget, ingredient)
            self.update_text_by_data(self.recipe_ingredient_list, ingredient)

    def on_item_deleted(self, item: Schema.Item):
        self.recipe_check_filter(item)
        self.recipe_update_lists()

    def on_recipe_clear_filters(self):
        self.recipe_filter = None

        self.recipe_filter_label.setText('No filter')
        self.recipe_clear_filters.setDisabled(True)
        self.recipe_filter_checkbox_creates.setDisabled(True)
        self.recipe_filter_checkbox_uses.setDisabled(True)

        self.recipe_update_lists()

    def on_recipe_filter_checked(self):
        if self.recipe_filter:
            self.recipe_update_lists()

    def on_recipe_tree_item_selected(self, tree_item: QTreeWidgetItem):
        self.recipe_set_enabled(False)

        if tree_item:
            if (recipe := self.current_recipe()) is not self.previous_recipe:
                self.previous_recipe = recipe
                self.recipe_display(recipe)

            self.recipe_product.setEnabled(True)
            self.recipe_product_amount.setEnabled(True)

            self.recipe_ingredient_list.setEnabled(True)
            self.recipe_workbench_list.setEnabled(True)

            if data := tree_item.data(0, Qt.UserRole):
                if isinstance(data, Schema.Ingredient):
                    self.select_entry_with_data(self.recipe_ingredient_list, data)

                elif isinstance(data, Schema.RecipeWorkbench):
                    self.select_entry_with_data(self.recipe_workbench_list, data)

            if self.recipe_ingredient_list.count() > 0 and self.recipe_ingredient_list.currentRow() != -1:
                self.recipe_ingredient.setEnabled(True)
                self.recipe_ingredient_amount.setEnabled(True)

            if self.recipe_workbench_list.count() > 0 and self.recipe_workbench_list.currentRow() != -1:
                self.recipe_workbench.setEnabled(True)

    def on_recipe_tree_item_right_clicked(self, point: QPoint):
        i_action, tree_item = self.exec_item_widget_menu(self.recipe_tree_widget, point)

        if i_action not in (0, 1):
            return

        data = tree_item.data(0, Qt.UserRole) if tree_item else None

        if tree_item is None or isinstance(data, Schema.Recipe):
            entity_type = Schema.Recipe
        elif isinstance(data, Schema.Ingredient) or (data is None and tree_item.text(0) == 'Ingredients'):
            entity_type = Schema.Ingredient
        # elif isinstance(data, Schema.RecipeWorkbench) or (data is None and tree_item.text(0) == 'Workbenches'):
        #     entity_type = Schema.RecipeWorkbench
        else:
            entity_type = Schema.RecipeWorkbench

        if i_action == 0:
            if entity_type is Schema.Recipe:
                self.recipe_new()
            elif entity_type is Schema.Ingredient:
                self.recipe_ingredient_new()
            elif entity_type is Schema.RecipeWorkbench:
                self.recipe_workbench_new()

        elif i_action == 1:
            if entity_type is Schema.Recipe:
                self.recipe_delete(data)
            elif entity_type is Schema.Ingredient:
                self.recipe_ingredient_delete(data)
            elif entity_type is Schema.RecipeWorkbench:
                self.recipe_workbench_delete(data)

    def on_recipe_ingredient_selected(self, list_item: QListWidgetItem):
        self.recipe_ingredient.setEnabled(False)
        self.recipe_ingredient_amount.setEnabled(False)

        if list_item:
            ingredient = list_item.data(Qt.UserRole)

            self.select_entry_with_data(self.recipe_ingredient, ingredient.item)
            self.recipe_ingredient_amount.setValue(ingredient.amount)

            self.recipe_ingredient.setEnabled(True)
            self.recipe_ingredient_amount.setEnabled(True)

    def on_recipe_ingredient_right_clicked(self, point: QPoint):
        i_action, list_item = self.exec_item_widget_menu(self.recipe_ingredient_list, point)

        if i_action == 0:
            self.recipe_ingredient_new()
        elif i_action == 1:
            self.recipe_ingredient_delete(list_item.data(Qt.UserRole))

    def on_recipe_workbench_selected(self, list_item: QListWidgetItem):
        self.recipe_workbench.setEnabled(False)

        if list_item:
            recipe_workbench = list_item.data(Qt.UserRole)

            self.select_entry_with_data(self.recipe_workbench, recipe_workbench.workbench)

            self.recipe_workbench.setEnabled(True)

    def on_recipe_workbench_right_clicked(self, point: QPoint):
        i_action, list_item = self.exec_item_widget_menu(self.recipe_workbench_list, point)

        if i_action == 0:
            self.recipe_workbench_new()
        elif i_action == 1:
            self.recipe_workbench_delete(list_item.data(Qt.UserRole))

    def on_recipe_product_changed(self):
        if self.recipe_product.isEnabled():
            recipe = self.current_recipe()
            if recipe:
                recipe.product = self.recipe_product.currentData()

                self.current_top_level().setText(0, str(recipe))

    def on_recipe_product_amount_changed(self, value: int):
        if self.recipe_product_amount.isEnabled():
            recipe = self.current_recipe()
            if recipe:
                recipe.product_amount = value

                self.current_top_level().setText(0, str(recipe))

    def on_recipe_ingredient_changed(self):
        if self.recipe_ingredient.isEnabled():
            ingredient = self.current_ingredient()
            if ingredient:
                ingredient.item = self.recipe_ingredient.currentData()

                self.recipe_ingredient_list.currentItem().setText(str(ingredient))
                index = self.recipe_ingredient_list.currentRow()
                self.current_top_level().child(0).child(index).setText(0, str(ingredient))

    def on_recipe_ingredient_amount_changed(self, value: int):
        if self.recipe_ingredient_amount.isEnabled():
            ingredient = self.current_ingredient()
            if ingredient:
                ingredient.amount = value

                self.recipe_ingredient_list.currentItem().setText(str(ingredient))
                index = self.recipe_ingredient_list.currentRow()
                self.current_top_level().child(0).child(index).setText(0, str(ingredient))

    def on_recipe_workbench_changed(self):
        if self.recipe_workbench.isEnabled():
            recipe_workbench = self.current_recipe_workbench()
            if recipe_workbench:
                recipe_workbench.workbench = self.recipe_workbench.currentData()

                self.recipe_workbench_list.currentItem().setText(str(recipe_workbench))
                index = self.recipe_workbench_list.currentRow()
                self.current_top_level().child(1).child(index).setText(0, str(recipe_workbench))
    # -------------------------

    # menu slots --------------
    def on_menu_save(self):
        if self.DB.Tables.modified():
            self.DB.Tables.apply()

            self.statusbar.showMessage('Changes saved', 5000)

    def on_menu_revert(self):
        if self.DB.Tables.modified():
            response = QMessageBox.question(self, 'Revert?', 'Revert changes?', QMessageBox.Yes | QMessageBox.No)

            if response == QMessageBox.Yes:
                self.DB.Tables.revert()

                self.previous_recipe = None

                self.item_update_lists()
                self.recipe_update_lists()

                self.statusbar.showMessage('Changes reverted', 5000)
    # -------------------------
    # -------------------------

    # region queries ----------
    def init_queries(self):
        self.actionQueryRecipes.triggered.connect(self.on_query_recipes)
        self.actionQueryWorkbenches.triggered.connect(self.on_query_workbenches)

    def on_query_recipes(self):
        used_in = self.DB.Query.execute(0, session=self.DB.Tables.session, id=self.current_item().id)

        display_list = [f'{amount} in "{name}"' for name, amount in used_in]

        if display_list:
            display_list = ['Recipes found:'] + display_list
        else:
            display_list += ['No recipes found']

        ResultDialog(self, display_list)

    def on_query_workbenches(self):
        item = self.current_item()
        if item.used_as:
            display_list = []
            for use in item.used_as:
                query_result = self.DB.Query.execute(1, session=self.DB.Tables.session, id=use.workbench.id)
                display_list += [row.name for row in query_result]

            if display_list:
                display_list = [f'Items made on {item.name}:'] + display_list
            else:
                display_list += ['No recipes found']

            ResultDialog(self, display_list)

        else:
            QMessageBox.information(self, 'No uses', 'This item is not a workbench')
    # -------------------------

    # region useful_stuff -----
    @staticmethod
    def select_entry_with_data(widget, data):
        if isinstance(widget, QListWidget):
            for index in range(widget.count()):
                if widget.item(index).data(Qt.UserRole) is data:
                    widget.setCurrentRow(index)
                    return index
        elif isinstance(widget, QTreeWidget):
            iterator = QTreeWidgetItemIterator(widget)
            while iterator.value():
                tree_item = iterator.value()
                if tree_item.data(0, Qt.UserRole) is data:
                    widget.setCurrentItem(tree_item)
                    return widget.invisibleRootItem().indexOfChild(tree_item)
                iterator += 1
        elif isinstance(widget, QComboBox):
            index = widget.findData(data)
            widget.setCurrentIndex(index)
            return index

    @staticmethod
    def update_text_by_data(widget, data):
        if isinstance(widget, QListWidget):
            for index in range(widget.count()):
                if widget.item(index).data(Qt.UserRole) is data:
                    widget.item(index).setText(str(data))
        elif isinstance(widget, QTreeWidget):
            iterator = QTreeWidgetItemIterator(widget)
            while iterator.value():
                tree_item = iterator.value()
                if tree_item.data(0, Qt.UserRole) is data:
                    tree_item.setText(0, str(data))
                iterator += 1
        elif isinstance(widget, QComboBox):
            index = widget.findData(data)
            widget.setItemText(index, str(data))

    @staticmethod
    def remove_by_data(widget, data):
        if isinstance(widget, QListWidget):
            for index in range(widget.count()):
                if widget.item(index).data(Qt.UserRole) is data:
                    widget.takeItem(index)
                    return
        elif isinstance(widget, QTreeWidget):
            iterator = QTreeWidgetItemIterator(widget)
            while iterator.value():
                tree_item = iterator.value()
                if tree_item.data(0, Qt.UserRole) is data:
                    tree_item.parent().removeChild(tree_item)
                    return
                iterator += 1
        elif isinstance(widget, QComboBox):
            index = widget.findData(data)
            widget.removeItem(index)
            return

    @staticmethod
    def right_click_menu(widget: QWidget, point: QPoint, actions: Iterable[QAction]):
        menu = QMenu(widget)

        for action in actions:
            menu.addAction(action)

        return menu.exec(widget.mapToGlobal(point))

    @staticmethod
    def get_widget_item_data(widget_item, role):
        if isinstance(widget_item, QListWidgetItem):
            return widget_item.data(role)
        elif isinstance(widget_item, QTreeWidgetItem):
            return widget_item.data(0, role)

    def exec_item_widget_menu(self, widget, point: QPoint) -> \
            Tuple[int, Union[QTreeWidgetItem, QListWidgetItem]]:

        widget_item = widget.itemAt(point)

        actions = [QAction('Add'), QAction('Delete')]
        if not widget_item or \
                (isinstance(widget_item, QTreeWidgetItem) and
                 widget_item.data(0, Qt.UserRole) is None):

            actions[1].setDisabled(True)

        action = self.right_click_menu(widget, point, actions)

        if action in actions:
            index = actions.index(action)
        else:
            index = -1

        return index, widget_item
    # -------------------------
