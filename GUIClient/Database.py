from sqlalchemy import create_engine, event
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm.session import Session as _Session
import json

from SchemaDefenition import Base
import SchemaDefenition as Schema

from typing import List, Tuple, Dict


Session = sessionmaker()


""" Session modification detection
    Due to flush resetting new/deleted/dirty lists
    fact of flushing need to be detected and tracked
"""
modified = 'modified'


@event.listens_for(Session, 'before_flush')
def set_flush(session, *_):
    """ If flush occurred on session created with Session,
        field 'modified' added to session info
    """

    session.info[modified] = has_changes(session)


@event.listens_for(Session, 'after_commit')
@event.listens_for(Session, 'after_rollback')
def reset_flush(session):
    """ If commit or rollback occurred field needs to be reset """

    if modified in session.info:
        del session.info[modified]


def has_changes(session):
    """ Checks if there is changes to be committed or rolled back """

    # Though it's unlikely, window can possibly pass None here
    if session is None:
        return False

    # Firstly, check for previous flush
    # Secondly, check if new or deleted records are present in session
    # Finally, check if there are (actually) modified records
    return session.info.get(modified, False) \
        or any(session.new) or any(session.deleted) \
        or any([o for o in session.dirty if session.is_modified(o)])


class prepare:
    """ Context manager to control session workflow
        Flushes existing session if provided
        Issues new and closes it afterwards if not
    """
    session = None
    none = True

    def __init__(self, session: _Session = None):
        self.none = session is None
        self.session = session or Session()

        self.session.flush()

    def __enter__(self) -> _Session:
        return self.session

    def __exit__(self, exc_type, exc_val, exc_tb):
        if self.none:
            self.session.close()


# This is where all database magic happens
class Database:
    """ Controls connection to database and query execution
        Manages engine object, Session and Base
    """
    config = {}     # Config dictionary
    url = ''        # Not sure why I'm keeping it, but it might be useful
    engine = None   # Our link to the data

    def __init__(self, *, echo: bool = False):
        self.load_config()
        self.connect(echo=echo)

    def load_config(self) -> None:
        # TODO: creation of sample config file and stuff
        with open('database.json') as config_file:
            self.config = json.load(config_file)

    def connect(self, *, echo: bool = False) -> None:
        # TODO: handling of connecting problem
        self.url = '{dbms}+{driver}://{username}:{password}@{host}/{database}'.format(**self.config)
        self.engine = create_engine(self.url, echo=echo)

        Session.configure(bind=self.engine)
        Base.prepare(self.engine, reflect=True)

    # raw connection methods. This ones are pretty universal, so if not for 'Form View' part, it can show you any database
    def get_table_names(self) -> List:
        with self.engine.connect() as connection:
            cursor = connection.execute('SHOW TABLES;')
            return [table[0] for table in cursor]

    def get_table_info(self, table_name: str) -> Tuple[Dict, List]:
        with self.engine.connect() as connection:
            table_info = {'row_count': 0, 'keys': []}

            cursor = connection.execute(f'SELECT COUNT(*) FROM {table_name};')
            table_info['row_count'] = cursor.fetchone()[0]

            columns_info = connection.execute(f'DESCRIBE {table_name};').fetchall()
            table_info['keys'] = [item for item in columns_info if item['Key'] == 'PRI']

            return table_info, columns_info

    def fetch_from_table(self, table_name: str, count: int = 100, skip: int = 0) -> List:
        with self.engine.connect() as connection:
            cursor = connection.execute(f'SELECT * FROM {table_name} LIMIT {skip}, {count};')
            return cursor.fetchall()

    class Tables:
        """ Main ORM component of Database class
            Manages ORM session and table contents querying
        """
        session: _Session = None
        queries = {}

        # Flow control
        @classmethod
        def apply(cls) -> None:
            if cls.session:
                cls.session.commit()

        @classmethod
        def revert(cls) -> None:
            if cls.session:
                cls.session.rollback()

        @classmethod
        def modified(cls) -> bool:
            # First take. Apparently, any assigning to field makes object dirty (even if nothing changed),
            # so this is no use. Also no check if session is None

            # return bool(len(cls.session.dirty) + len(cls.session.new) + len(cls.session.deleted))

            # Second take. Now we check if object was actually modified if it is dirty.
            # But. Flushing session clears lists, despite there was no commit,
            # so after flush session is treated as not modified

            # if cls.session is None:
            #     return False
            #
            # return any(cls.session.new) or any(cls.session.deleted) or \
            #     any([x for x in cls.session.dirty if cls.session.is_modified(x)])

            ...

            # Third take. Requires some event action. Check at the top
            return has_changes(cls.session)

        # Universal methods
        @classmethod
        def add(cls, instance) -> None:
            if not cls.session:
                cls.session = Session()

            cls.session.add(instance)
            cls.session.flush()

        @classmethod
        def delete(cls, instance) -> None:
            # Not sure if should delete instance from other session in new one
            if cls.session:
                cls.session.delete(instance)

        @classmethod
        def refresh(cls, instance) -> None:
            if not cls.session:
                cls.session = Session()

            cls.session.refresh(instance)

        @classmethod
        def getTable(cls, _type) -> List:
            if _type not in cls.queries:
                if not cls.session:
                    cls.session = Session()

                cls.queries[_type] = cls.session.query(_type)

            return cls.queries[_type].all()

        # Table-specific methods
        @classmethod
        def Items(cls) -> List[Schema.Item]:
            return cls.getTable(Schema.Item)

        @classmethod
        def Recipes(cls) -> List[Schema.Recipe]:
            return cls.getTable(Schema.Recipe)

        @classmethod
        def Workbenches(cls) -> List[Schema.Workbench]:
            return cls.getTable(Schema.Workbench)

        @classmethod
        def Types(cls) -> List[Schema.ItemType]:
            return cls.getTable(Schema.ItemType)

        @classmethod
        def Rarities(cls) -> List[Schema.Rarity]:
            return cls.getTable(Schema.Rarity)

        @classmethod
        def StackSizes(cls) -> List[Schema.StackSize]:
            return cls.getTable(Schema.StackSize)

        @classmethod
        def DeleteItem(cls, item: Schema.Item) -> bool:
            if item.used_in:
                return False

            cls.delete(item)

            return item in cls.session.deleted

    class Query:
        """ Auxiliary class to execute queries """
        queries = [
            '''SELECT name, amount FROM
                    items JOIN recipes ON items.id = product_id
                        JOIN recipes_to_ingredients ON recipes.id = recipe_id
                            WHERE item_id = :id;''',

            '''SELECT DISTINCT items.id, items.name AS name FROM
                    items JOIN recipes ON product_id = items.id
                        JOIN recipes_to_workbenches ON recipes.id = recipe_id
                            WHERE workbench_id = :id;'''
        ]

        @classmethod
        def execute(cls, index, *, session: _Session = None, **kwargs):
            with prepare(session) as s:
                return s.execute(cls.queries[index], kwargs)


if __name__ == '__main__':
    d = Database(echo=False)
    res = d.Query.execute(0, id=2)

    _items = d.Tables.Items()
